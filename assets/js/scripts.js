function mostrar_exito(o){
    var status = (o.creado)?'creado':'actualizado';
        swal('Éxito!','Se ha '+status+' la estación correctamente.',
  'success');
        window.location.href = RootJS+'admin';
}

$(document).ready(function() {

    $(document).on('click','.secondary-sidebar li', function(e){
        e.preventDefault();
        $(this).toggleClass('open active');
        if(!$(this).hasClass('is-child')){
            $('.secondary-sidebar li').not(this).removeClass('open active');
        }
        $(this).find('arrow').toggleClass('open');
    });

    $(document).on('click', '#doc-carga', function(e){
		e.preventDefault();
		$('#file').trigger('click');
	});

    $(document).on('click', '.doc-carga', function(e){
		e.preventDefault();
		$('#file').trigger('click');
	});

    $(document).on('click', '.documento-link', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var id_estacion = $('#id_estacion').val();
        $('#right-content').html('');
        /*$('#doc-archivo').hide();
        $('#doc-nombre').hide();
        $('#doc-descripcion').hide();
        $('#doc-carga').hide();
        $('#doc-video').hide();
        $('#mas-info').hide();*/
        $.post(RootJS+"admin/estaciones/get_item", {id:id, id_estacion:id_estacion}, function(data, status){
            $('#right-content').append(data);
            /*var res = JSON.parse(data);
            if(res.result){
                $('#doc-nombre').html(res.item.nombre);

                $('#doc-descripcion').html(res.item.descripcion);
                $('#doc-carga').attr('href', '#');
                $('#id_documento').val(res.item.id_documento);
                $('#doc-video').attr('href','https://www.youtube.com/watch?v=XE9Wz38rX0Q');
                $()
                $('#doc-nombre').show();
                $('#doc-descripcion').show()
                if(res.item.url == ''){
                    $('#doc-carga').show();
                    $('#doc-video').show();
                }else{
                    $('#doc-file').attr('href', res.path+res.item.url);
                    $('#doc-archivo').show();
                }

                if(res.item.url_mas_info != ''){
                    $('#mas-info').attr('href', res.item.url_mas_info);
                    $('#mas-info').show();
                }
            }*/
        });
    });


    $(document).on('change', '#file', function(e){
		e.preventDefault();
		var self = $('#upload-file');
        var url = self.attr('action');
        self.addClass("disabled");
        var datos = new FormData( self[0] );
        $.ajax({
            'url': url,
            type: 'post',
            data: datos,
	        dataType:"json",
	        cache: false,
	        processData: false,
	        contentType: false,
            complete: function (o) {
                self.removeClass("disabled");
            },
            success: function (o) {
                if (o.result == 1) {
                    $('#doc-carga').hide();
                    $('#doc-video').hide();
                    $('#doc-archivo').show();
                    $('#doc-file').attr('href', res.item.url);

                } else {
                    Result.showError(o.error);
                }
            }
        });
	});

    $(document).on('change', '#file-child', function(e){
		e.preventDefault();
		var self = $('#upload-file');
        var url = self.attr('action');
        self.addClass("disabled");
        var datos = new FormData( self[0] );
        $.ajax({
            'url': url,
            type: 'post',
            data: datos,
	        dataType:"json",
	        cache: false,
	        processData: false,
	        contentType: false,
            complete: function (o) {
                self.removeClass("disabled");
            },
            success: function (o) {
                if (o.result == 1) {
                    $('#doc-carga').hide();
                    $('#doc-video').hide();
                    $('#doc-archivo').show();
                    $('#doc-file').attr('href', res.item.url);

                } else {
                    Result.showError(o.error);
                }
            }
        });
	});
});

$(document).on('click', '.delete-file', function(e){
    e.preventDefault();
    var id = $(this).data('id');
    console.log(id);
    swal({
      title: '¿Realmente desea borrar el archivo?',
      text: "No será posible deshacer esta acción.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminalo!',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        swal(
          'Eliminado!',
          'Su archivo ha sido eliminado correctamente',
          'success'
        )
      }
    });
});