<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages']   = [];

$autoload['libraries']  = ['database','session','form_validation','user_agent', 'cart'];

$autoload['drivers']    = [];

$autoload['helper']     = ['url','view','text'];

$autoload['config']     = [];

$autoload['language']   = [];

$autoload['model']      = [];
