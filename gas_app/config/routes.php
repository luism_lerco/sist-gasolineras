<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller']    = 'inicio';
//$route['404_override']          = 'inicio/error';
$route['registro']              = 'inicio/registro';
$route['admin']                 = 'admin/inicio';
$route['admin/mi-perfil']       = 'admin/mi_perfil';
$route['translate_uri_dashes']      = FALSE;


$route['admin/salir']               = "admin/logout";

$route['admin/estaciones/(:num)/informacion-general']   = 'admin/estaciones/info_general/$1';
$route['admin/estaciones/(:num)/autoridades']           = 'admin/estaciones/autoridades/$1';