<?php

class API_CONTROLLER extends CI_Controller {

    //------------------------------------------------------------------------------------------------------------------------------
    // GENÉRICAS
    //------------------------------------------------------------------------------------------------------------------------------
    public function get($modelo, $id = NULL){
        $this->load->model($modelo);
        if ($id != NULL) {
            $result = $this->$modelo->get($id);
        } else {
            $result = $this->$modelo->get();
        }
        $this->output->set_output(json_encode($result));
        return false;
    }

    //------------------------------------------------------------------------------------------------------------------------------
    public function get_sorted($modelo, $id = NULL) {
        $this->load->model($modelo);
        if ($id != NULL) {
            $result = $this->$modelo->get($id);
        } else {
            if ($this->input->post('q')) {
                $queries = json_decode($this->input->post('q'), true);
                foreach ($queries as $i => $v) {
                    $this->db->where($i, $v);
                }
            }
            $result = $this->$modelo->get(array('eliminado' => 0), 'orden');
        }
        $this->output->set_output(json_encode($result));
        return false;
    }

    //------------------------------------------------------------------------------------------------------------------------------
    public function update_activo($modelo, $id) {
        $this->load->model($modelo);
        $this->$modelo->update(array(
            'activo' => (int) $this->input->post('activo')
                ), $id);
        $this->output->set_output(json_encode(array(
            'result' => 1
        )));
        return false;
    }

    //------------------------------------------------------------------------------------------------------------------------------
    public function sort($modelo) {
        $this->load->model($modelo);
        foreach ($this->input->post('sort') as $index => $value) {
            $this->$modelo->update(array('orden' => $index), $value);
        }
        $this->output->set_output(json_encode(array(
            'result' => 1
        )));
        return false;
    }

    //------------------------------------------------------------------------------------------------------------------------------
    public function delete($modelo, $id) {
        $this->load->model($modelo);
        $this->$modelo->update(array(
            'eliminado' => 1,
                ), $id);
        $this->output->set_output(json_encode(array(
            'result' => 1
        )));
    }

    //------------------------------------------------------------------------------------------------------------------------------
    public function update_textfield($modelo, $id, $campo) {
        $this->load->model($modelo);
        $this->$modelo->update(array(
            $campo => $this->input->post('valor'),
                ), $id);
        $this->output->set_output(json_encode(array(
            'result' => 1,
            'valor' => $this->input->post('valor'),
            'id' => $id
        )));
    }

}

class ADMIN_Controller extends API_Controller {

    //------------------------------------------------------------------------------------------------------------------------------
    public $TIPOS_DE_USUARIO;
    public $usuario_actual;
    public $dependencias;
    public $estaciones;

    function __construct() {
        parent::__construct();

        $this->load->model(['grupos_model']);

        $groups = $this->session->userdata('grupos');

        if(!is_array($groups)){
            redirect(base_url('admin/login'),'refresh');
        }

        $grupos = array_column($groups,'id_grupo');

        if(!$this->session->userdata('id_usuario')  || ( array_search(SUPER_ADMINISTRADOR, $grupos) !== FALSE && array_search(PRINCIPAL, $grupos) !== FALSE && array_search(OPERADOR, $grupos) !== FALSE  && array_search(USUARIO, $grupos) !== FALSE) ){
            redirect(base_url('admin/login'),'refresh');
        }
        $this->usuario_actual = $this->session->userdata('id_usuario');

        $usuarios             = $this->grupos_model->get();
        $this->TIPOS_DE_USUARIO = [];
        foreach($usuarios as $u){
            $this->TIPOS_DE_USUARIO[] = ['id' => $u['id_grupo'],  'label' => $u['nombre'] ];
        }

        //Se cargan todas las depedencias
        $this->load->model(['dependencias_model']);
        $this->dependencias = $this->dependencias_model->get(['activo' => '1', 'eliminado' => '0'], 'orden');

        //Se cargan todas las estaciones del usuario
        $this->load->model(['estaciones_model','usuarios_has_estaciones_model']);
        $uhe = $this->usuarios_has_estaciones_model->get([
            'id_usuario' => $this->usuario_actual,
            'usuarios_has_estaciones.activo' => '1'
        ]);

        if(!empty($uhe)){
            $this->estaciones = [];
            foreach($uhe as $i){
                $tmp = $this->estaciones_model->get([
                    'id_estacion'   => $i['id_estacion'],
                    'activo'        => '1',
                    'eliminado'     => '0'
                ]);

                $this->estaciones[] = $tmp[0];
            }
        }
    }
}

