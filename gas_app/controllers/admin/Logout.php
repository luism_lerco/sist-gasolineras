<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {
    //-----------------------------------------------------------------------------------------------------------
    public function index() {
        redirect(base_url('admin/login'),'refresh');
    }
}