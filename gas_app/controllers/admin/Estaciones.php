<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Estaciones extends ADMIN_Controller {

    //----------------------------------------------------- -------------------------------------------------------------------------
    public function index() {
        dashboard_render('admin/inicio');
    }

    public function nueva(){
        $data   = ['url_form' => base_url('apis/admin_api/insertar_estacion')];
        $hdata  = ['full_height' => TRUE];
        $fdata  = ['link_active' => ['liEstaciones', 'lkNueva']];
        dashboard_render('admin/estaciones/form_view', $data, $hdata, $fdata);
    }

    public function autoridades($id){
        $this->load->model(['documentos_model', 'estaciones_model']);
        $estacion = $this->estaciones_model->get(['id_estacion' => $id, 'activo'=> '1', 'eliminado' => '0']);
        $breadcumb = [['label' => 'Inicio', 'url' => base_url('admin/inicio')], ['label'=> 'Estaciones'], ['label' => $estacion[0]['nombre']], ['label' => 'Autoridades']];
        $hdata  = ['id_estacion' => $id, 'estacion' => $estacion[0], 'breadcumb' => $breadcumb];
        $fdata  = ['link_active' => ['liAutoridades'], 'full_height' => TRUE];
        $dependencias = $this->dependencias;
        foreach($dependencias as &$e){
            $docs = $this->documentos_model->get(['id_dependencia' => $e['id_dependencia'], 'activo' => '1', 'eliminado' => '0']);
            foreach($docs as &$d){
                $hijos = $this->documentos_model->get(['id_dependencia' => $e['id_dependencia'], 'activo' => '1', 'eliminado' => '0', 'id_padre' => $d['id_documento']]);
                $d['hijos'] = $hijos;
            }
            $e['documentos'] = $docs;
        }
        $data   = ['dependencias' => $dependencias, 'id_estacion' => $id];
        dashboard_render('admin/estaciones/documentos_view', $data, $hdata, $fdata);
    }

    public function info_general($id){
        $this->load->model(['estaciones_model']);
        $estacion = $this->estaciones_model->get(['id_estacion' => $id, 'activo'=> '1', 'eliminado' => '0']);
        $breadcumb = [['label' => 'Inicio', 'url' => base_url('admin/inicio')], ['label'=> 'Estaciones'], ['label' => $estacion[0]['nombre']], ['label' => 'Información Gral.']];
        $data   = [
            'item'      => $this->estaciones_model->get(['id_estacion' => $id]),
            'url_form'  => base_url('apis/admin_api/update_estacion')

        ];
        $hdata  = [ 'id_estacion' => $id, 'estacion' => $estacion[0], 'breadcumb' => $breadcumb];
        $fdata  = ['link_active' => ['liGeneral']];
        dashboard_render('admin/estaciones/form_view', $data, $hdata, $fdata);
    }

    public function get_item(){
        $id             = $this->input->post('id');
        $id_estacion    = $this->input->post('id_estacion');
        if(intval($id) > 0){
            $this->load->model(['documentos_model', 'estaciones_has_documentos_model']);
            $item = $this->estaciones_has_documentos_model->get([
                //'OR estaciones_has_documentos.id_estacion' => $id_estacion,
                'documentos.id_documento'               => $id,
                'documentos.activo'                     => '1',
                'documentos.eliminado'                  => '0'
            ]);

            $doc_count = $this->documentos_model->count([
                'id_padre'      => $id,
                'activo'        => '1',
                'eliminado'     => '0'
            ]);

            $folder = DOCUMENTOS_PATH.sha1($id_estacion).'/documentos/';
            $path = base_url($folder);

            $data['item']           = $item[0];
            $data['path']           = $path;
            $data['id_estacion']    = $id_estacion;

            if($doc_count > 0){
                $data['items'] = $this->estaciones_has_documentos_model->get([
                    'id_padre'      => $id,
                    'documentos.activo'        => '1',
                    'documentos.eliminado'     => '0'
                ]);
                echo $this->load->view('admin/estaciones/documentos_detalle_hijos_view', $data, true);
                return false;
            }else{
                echo $this->load->view('admin/estaciones/documentos_detalle_view', $data, true);
                return false;
            }

            /*$this->output->set_output(json_encode([
                'path'   => $path,
                'item'   => $item[0],
                'result' => 1
            ]));
            return false;*/
        }
        $this->output->set_output(json_encode([
            'result' => 0
        ]));
        return false;
    }
}
?>