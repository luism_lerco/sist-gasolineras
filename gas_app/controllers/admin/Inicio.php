<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicio extends ADMIN_Controller {

    //----------------------------------------------------- -------------------------------------------------------------------------
    public function index() {
        $this->load->model(['estaciones_model','usuarios_has_estaciones_model','documentos_model', 'estaciones_has_documentos_model']);

        $documentos = $this->documentos_model->count();

        $uhe = $this->usuarios_has_estaciones_model->get([
            'id_usuario' => $this->usuario_actual,
            'usuarios_has_estaciones.activo' => '1'
        ]);

        if(!empty($uhe)){
            $estaciones = [];
            foreach($uhe as $key => $i){
                $tmp = $this->estaciones_model->get([
                    'id_estacion' => $i['id_estacion'],
                    'activo' => '1',
                    'eliminado' => '0'
                ]);

                $mis_docs = $this->estaciones_has_documentos_model->count(['id_estacion' => $i['id_estacion']]);

                $tmp[0]['total_documentos'] = $documentos;
                $tmp[0]['mis_documentos']   = $mis_docs;

                $g_estaciones[] = ['id' => 'estacion_'.$i['id_estacion'], 'total' => $documentos, 'mis_docs' => $mis_docs];

                $estaciones[] = $tmp[0];

            }

            $hdata = ['css' => $this->load->view('admin/assets/css/dashboard_inicio'), 'full_width' => true];
            $fdata = ['js' => $this->load->view('admin/assets/js/dashboard_inicio', ['graficas' => $g_estaciones], TRUE)];
            dashboard_render('admin/inicio',$hdata, ['estaciones' => $estaciones], $fdata);
        }
    }
}
?>