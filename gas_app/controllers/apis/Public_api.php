<?php

class Public_api extends API_CONTROLLER{ //ADMIN_Controller {

    function __construct() {
        parent::__construct();
        if( ! ini_get('date.timezone') )
        {
           date_default_timezone_set('America/Mexico_City');
        }
    }

    //------------------------------------------------------------------------------
    public function login() {
        error_reporting(-1);
        $this->form_validation->set_rules('usuario', 'Usuario', 'trim|required');
        $this->form_validation->set_rules('password', 'Contraseña', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        $this->load->model(['usuarios_model','usuarios_has_estaciones_model', 'grupos_model']);
        $where = [
            'email'     => $this->input->post('usuario'),
            'password'  => sha1($this->input->post('password')),
            'eliminado' => 0
        ];
        $usuario = $this->usuarios_model->get($where);

        if (count($usuario) == 0) {
            $this->output->set_output(json_encode(array(
                'result'    => 0,
                'error'     => 'Usuario y/o contraseña incorrectos'
            )));
            return false;
        }

        if($usuario[0]['activo'] == '0'){
            $this->output->set_output(json_encode(array(
                'result'    => 0,
                'error'     => 'El usuario se encuentra <b>inactivo</b>, contacte al administrador'
            )));
            return false;
        }

        $session = [
            'id_usuario'    => $usuario[0]['id_usuario'],
            'nombre'        => $usuario[0]['nombre'],
            'paterno'       => $usuario[0]['paterno'],
            'materno'       => $usuario[0]['materno'],
            'usuario'       => $usuario[0]['email'],
            'grupos'        => $this->usuarios_has_estaciones_model->get([
                'id_usuario'                                => $usuario[0]['id_usuario'],
                'usuarios_has_estaciones.activo'            => 1
            ]),
        ];

        $this->session->set_userdata($session);

        $this->output->set_output(json_encode(array(
            'result' => 1,
        )));
    }

}