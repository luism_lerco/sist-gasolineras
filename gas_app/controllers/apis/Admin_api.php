<?php

class Admin_api extends ADMIN_Controller {

    function __construct() {
        parent::__construct();
        if( ! ini_get('date.timezone') )
        {
           date_default_timezone_set('America/Mexico_City');
        }
    }

    //------------------------------------------------------------------------------
    public function insertar_estacion(){
        //Validacion de Form
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('no_permiso', 'No. Permiso de la CRE', 'trim|required');
        $this->form_validation->set_rules('rfc', 'R.F.C.', 'trim|required');
        $this->form_validation->set_rules('razon_social', 'Razón Social', 'trim|required');
        $this->form_validation->set_rules('nombre_representante', 'Nombre representante', 'trim|required');
        $this->form_validation->set_rules('nombre_encargado', 'Nombre encargado', 'trim|required');
        $this->form_validation->set_rules('calle', 'Calle', 'trim|required');
        $this->form_validation->set_rules('no_ext', 'No.Ext', 'trim|required');
        $this->form_validation->set_rules('no_int', 'No.Int', 'trim');
        $this->form_validation->set_rules('cp', 'C.P.', 'trim|required');
        $this->form_validation->set_rules('colonia', 'Colonia', 'trim|required');
        $this->form_validation->set_rules('municipio', 'Municipio', 'trim|required');
        $this->form_validation->set_rules('estado', 'Estado', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        //Validación del correo único
        $this->load->model(['estaciones_model', 'usuarios_has_estaciones_model']);

        $data = [
            'nombre'                => $this->input->post('nombre'),
            'no_permiso'            => $this->input->post('no_permiso'),
            'rfc'                   => $this->input->post('rfc'),
            'razon_social'          => $this->input->post('razon_social'),
            'nombre_representante'  => $this->input->post('nombre_representante'),
            'nombre_encargado'      => $this->input->post('nombre_encargado'),
            'calle'                 => $this->input->post('calle'),
            'no_ext'                => $this->input->post('no_ext'),
            'no_int'                => $this->input->post('no_int'),
            'cp'                    => $this->input->post('cp'),
            'colonia'               => $this->input->post('colonia'),
            'municipio'             => $this->input->post('municipio'),
            'estado'                => $this->input->post('estado'),
            'fecha_creacion'        => time(),
            'creado_por'            => $this->usuario_actual,
            'eliminado_por'         => $this->usuario_actual,
            'activo'                => '1'
        ];

        $result = $this->estaciones_model->insert($data);

        if ($result) {
            $this->usuarios_has_estaciones_model->insert([
                'id_usuario'    => $this->usuario_actual,
                'id_estacion'   => $result,
                'id_grupo'      => '1',
                'activo'        => '1'
            ]);

            $this->output->set_output(json_encode(array(
                'creado' => 1,
                'result' => 1
            )));
            return false;
        }

        $this->output->set_output(json_encode(array(
            'result' => 0
        )));
        return false;
    }

    function update_estacion(){
        //Validacion de Form
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('no_permiso', 'No. Permiso de la CRE', 'trim|required');
        $this->form_validation->set_rules('rfc', 'R.F.C.', 'trim|required');
        $this->form_validation->set_rules('razon_social', 'Razón Social', 'trim|required');
        $this->form_validation->set_rules('nombre_representante', 'Nombre representante', 'trim|required');
        $this->form_validation->set_rules('nombre_encargado', 'Nombre encargado', 'trim|required');
        $this->form_validation->set_rules('calle', 'Calle', 'trim|required');
        $this->form_validation->set_rules('no_ext', 'No.Ext', 'trim|required');
        $this->form_validation->set_rules('no_int', 'No.Int', 'trim');
        $this->form_validation->set_rules('cp', 'C.P.', 'trim|required');
        $this->form_validation->set_rules('colonia', 'Colonia', 'trim|required');
        $this->form_validation->set_rules('municipio', 'Municipio', 'trim|required');
        $this->form_validation->set_rules('estado', 'Estado', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        //Validación del correo único
        $this->load->model(['estaciones_model', 'usuarios_has_estaciones_model']);

        $data = [
            'nombre'                => $this->input->post('nombre'),
            'no_prmiso'           => $this->input->post('no_permiso'),
            'rfc'                   => $this->input->post('rfc'),
            'razon_social'          => $this->input->post('razon_social'),
            'nombre_representante'  => $this->input->post('nombre_representante'),
            'nombre_encargado'      => $this->input->post('nombre_encargado'),
            'calle'                 => $this->input->post('calle'),
            'no_ext'                => $this->input->post('no_ext'),
            'no_int'                => $this->input->post('no_int'),
            'cp'                    => $this->input->post('cp'),
            'colonia'               => $this->input->post('colonia'),
            'municipio'             => $this->input->post('municipio'),
            'estado'                => $this->input->post('estado'),
            'fecha_creacion'        => time(),
            'creado_por'            => $this->usuario_actual,
            'eliminado_por'         => $this->usuario_actual,
            'activo'                => '1'
        ];

        $this->estaciones_model->update($data, $this->input->post('id'));
        $this->output->set_output(json_encode(array(
            'creado' => 0,
            'result' => 1
        )));
    }

    function subir_documento(){
        $this->load->model(['estaciones_has_documentos_model']);

	    $id_documento = $this->input->post('id_documento');
	    $id_estacion  = $this->input->post('id_estacion');

	    $f1 = DOCUMENTOS_PATH.sha1($id_estacion);

	    if(!file_exists($f1)){ mkdir($f1, 0777); }

	    $folder = $f1.'/documentos/';

		// If folder not exists, we created

	    if (!file_exists($folder)) { mkdir($folder, 0777); }

	    $config['upload_path']          = $folder;
	    $config['allowed_types']        = 'pdf';
	    $config['max_size']             = 7000;
	    $config['file_ext_tolower']		= TRUE;
	    $config['file_name']			= time();

	    $this->load->library('upload', $config);

	    if ( ! $this->upload->do_upload('file'))
        {
            $error = array('error' => $this->upload->display_errors());
	        $this->output->set_output(json_encode(array(
	            'result' => 0,
	            'error' => 'No se pudo guardar el documento de la estación. Intentar nuevamente.'
	        )));
	        return false;
        }

        $data = ['upload_data' => $this->upload->data()];
        $info['url'] = $data['upload_data']['file_name'];
        $info['id_documento'] = $id_documento;
        $info['id_estacion']  = $id_estacion;
        $info['activo']       = '1';
        $this->estaciones_has_documentos_model->insert($info);

        $this->output->set_output(json_encode(array(
            'result'    => 1,
            'path' 		=> base_url($folder),
            'url'		=> $data['upload_data']['file_name'],
        )));
    }

}