<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Acceso - <?=NOMBRE_SITIO?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" sizes="57x57" href="<?=base_url('assets/img/favicon/apple-icon-57x57.png')?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=base_url('assets/img/favicon/apple-icon-60x60.png')?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=base_url('assets/img/favicon/apple-icon-72x72.png')?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url('assets/img/favicon/apple-icon-76x76.png')?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url('assets/img/favicon/apple-icon-114x114.png')?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=base_url('assets/img/favicon/apple-icon-120x120.png')?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=base_url('assets/img/favicon/apple-icon-144x144.png')?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=base_url('assets/img/favicon/apple-icon-152x152.png')?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=base_url('assets/img/favicon/apple-icon-180x180.png')?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?=base_url('assets/img/favicon/android-icon-192x192.png')?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=base_url('assets/img/favicon/favicon-32x32.png')?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?=base_url('assets/img/favicon/favicon-96x96.png')?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url('assets/img/favicon/favicon-16x16.png')?>">
    <link rel="manifest" href="<?=base_url('assets/img/favicon/manifest.json')?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?=base_url('assets/img/favicon/ms-icon-144x144.png')?>">
    <meta name="theme-color" content="#ffffff">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/pace/pace-theme-flash.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/font-awesome/css/font-awesome.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/jquery-scrollbar/jquery.scrollbar.css')?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/select2/css/select2.min.css')?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/switchery/css/switchery.min.css')?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/pages/css/pages-icons.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/pages/css/pages.css')?>" class="main-stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/login.css')?>" class="main-stylesheet" />

    <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="pages/css/windows.chrome.fix.css" />'
    }
    </script>
  </head>
  <body class="fixed-header ">
    <div class="login-wrapper ">
      <div class="bg-pic">
        <img src="<?=base_url('assets/img/procesos.jpg')?>" data-src="<?=base_url('assets/img/procesos.jpg')?>" data-src-retina="<?=base_url('assets/img/procesos.jpg')?>" alt="Procesos" class="lazy">
        <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
          <p class="small">
              Todos los derechos reservados © <?=date('Y')?> Grupo Petroaadlab.
          </p>
        </div>
      </div>
      <div class="login-container bg-white">
        <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
          <img src="<?=base_url('assets/img/logo.png')?>" alt="logo" data-src="<?=base_url('assets/img/logo.png')?>" data-src-retina="<?=base_url('assets/img/logo_2x.png')?>" width="78" height="22">
            <div id="login-alert" class="alert alert-danger" style="display:none"></div>
            <form id="loginform" action="<?= $URL_FORM ?>" method="POST"  class="p-t-15" role="form">
            <div class="form-group form-group-default">
              <label>Usuario</label>
              <div class="controls">
                <input type="text" name="usuario" placeholder="Nombre de Usuario" class="form-control" required>
              </div>
            </div>
            <!-- END Form Control-->
            <!-- START Form Control-->
            <div class="form-group form-group-default">
              <label>Contraseña</label>
              <div class="controls">
                <input type="password" class="form-control" name="password" placeholder="Contraseña" required>
              </div>
            </div>
            <!-- START Form Control-->
            <div class="row">
              <div class="col-md-12 d-flex align-items-center justify-content-start">
                <a href="#" class="text-info small">¿Has olvidado los datos de la cuenta?</a>
              </div>
            </div>
            <!-- END Form Control-->
            <div class="row">
                <div class="col-md-6"><a class="btn btn-link m-t-10" href="<?=base_url('registro')?>">Crear Cuenta</a></div>
                <div class="col-md-6"><button class="btn btn-primary btn-cons m-t-10 pull-right" type="submit">Entrar</button></div>
          </form>
          <!--END Login Form-->
        </div>
      </div>
      <!-- END Login Right Container-->
    </div>
    <script type="text/javascript" src="<?=base_url('assets/plugins/pace/pace.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery/jquery-1.11.1.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/modernizr.custom.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-ui/jquery-ui.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/tether/js/tether.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/bootstrap/js/bootstrap.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery/jquery-easy.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-unveil/jquery.unveil.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-ios-list/jquery.ioslist.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-actual/jquery.actual.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/select2/js/select2.full.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/classie/classie.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/switchery/js/switchery.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-validation/js/jquery.validate.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-validation/js/localization/messages_es.js')?>"></script>
    <!-- END VENDOR JS -->
    <script type="text/javascript" src="<?=base_url('assets/pages/js/pages.min.js')?>"></script>

    <script type="text/javascript" src="<?=base_url('assets/js/result.js') ?>"></script>
    <script>
        $(document).ready(function () {
            $('#loginform').validate();
            Result = new Result();
            $("#loginform").submit(function(event) {
                $("#login-alert").fadeOut();
                event.preventDefault();
                var url = $(this).attr('action');
                var postData = $(this).serialize();
                $.post(url, postData, function(o) {
                    if (o.result == 1) {
                        window.location.href = '<?= base_url('admin/inicio') ?>';
                    }
                    else {
                        Result.error(o.error,$("#login-alert"));
                    }
                }, 'json');
            });
        });
    </script>
  </body>
</html>