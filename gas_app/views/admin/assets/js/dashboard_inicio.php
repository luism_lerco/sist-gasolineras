<script type="text/javascript" src="<?=base_url('assets/plugins/full-calendar/lib/moment.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/plugins/full-calendar/fullcalendar.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/plugins/full-calendar/locale-all.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/plugins/jquery-sparkline/jquery.sparkline.min.js')?>"></script>
<script type="text/javascript">
    $( document ).ready(function() {
        <?php if(isset($graficas) && !empty($graficas) && (is_array($graficas) || is_object($graficas))):?>
        <?php foreach($graficas as $g):?>
        $("#<?=$g['id']?>").sparkline(
        [<?=$g['mis_docs']?>,<?=$g['total']-$g['mis_docs']?>], {
            type: 'pie',
            sliceColors: ['#008b70', '#F55753'],
            width: '100',
            height: '100'
        });
        <?php endforeach; ?>
        <?php endif;?>

        $('#carousel-inicio').carousel();
        var winWidth = $(window).innerWidth();
        $(window).resize(function () {

            if ($(window).innerWidth() < winWidth) {
                $('.carousel-inner>.item>img').css({
                    'min-width': winWidth, 'width': winWidth
                });
            }
            else {
                winWidth = $(window).innerWidth();
                $('.carousel-inner>.item>img').css({
                    'min-width': '', 'width': ''
                });
            }
        });

        <?php if(isset($graficas) && !empty($graficas) && (is_array($graficas) || is_object($graficas))):?>
        <?php foreach($graficas as $index => $g):?>
        $('#calendario-<?=$index?>').fullCalendar({
            locale: 'es',
        });
        <?php endforeach;?>
        <?php endif;?>
    });


</script>