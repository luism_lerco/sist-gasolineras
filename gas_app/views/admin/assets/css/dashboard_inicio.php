<link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/full-calendar/fullcalendar.min.css')?>" />
<style>
    .dashboard-header{ margin:0; }
    .carousel-inner > .item > img, .carousel-inner > .item > a > img {
        display: block;
        height: 400px;
        min-width: 100%;
        width: 100%;
        max-width: 100%;
        line-height: 1;
    }
</style>