<div class="col-8">
    <h2><?=$item['nombre']?></h2>
    <p><?=$item['descripcion']?></p>
    <?php if($item != FALSE && isset($item['url']) && !empty($item['url']) ):?>
    <div id="doc-archivo" class="row justify-content-center">
        <div class="col-5 text-center">
            <img class="img-fluid" src="<?=base_url('assets/img/doc.png')?>" />
            <div  id="doc-fecha_expiracion" class="">
                <a id="doc-file" data-fancybox="" class="btn btn-sm btn-success" href="<?=base_url(DOCUMENTOS_PATH.sha1($id_estacion).'/documentos/'.$item['url'])?>"><i class="fa fa-eye" aria-hidden="true"></i> Visualizar</a>
                <a class="btn btn-sm btn-default delete-file" data-id="<?=$item['id_documento']?>" data-estacion="<?=$id_estacion?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <?php else:?>
    <a id="doc-carga" class="btn btn-primary"><i class="fa fa-upload" aria-hidden="true"></i> Carga tu documento</a><br>
    <form id="upload-file"  method="post" action="<?=base_url('apis/admin_api/subir_documento')?>" class="ajaxPostForm" data-function-success="recargar_documento">
    	<input type="file" id="file" name="file" accept="pdf" style="display:none" />
    	<input type="hidden" name="id_documento" id="id_documento" value="" />
    	<input type="hidden" id="id_estacion" name="id_estacion" value="<?=$id_estacion?>" />
    	<input type="submit" style="display:none">
    </form>
    <a id="doc-video" data-fancybox="" class="btn btn-link"><i class="fa fa-question-circle" aria-hidden="true"></i> Ver Video de ayuda</a>
    <?php endif;?>
</div>
<div class="col-3 text-center">
    <a href="http://www.petroaadlab.com/" target="_blank"><img id="doc-video-img" class="img-fluid" src="<?=base_url('assets/img/petrolab.jpg')?>" /></a>
    <br>
    <br>
    <div class="text-center">
        <a id="mas-info" class="btn btn-primary" href="<?=$item['url_mas_info']?>" target="_blank">Más información</a>
    </div>
</div>