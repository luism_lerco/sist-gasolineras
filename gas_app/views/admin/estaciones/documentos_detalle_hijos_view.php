<div class="row justify-content-center m-t-30">
    <div class="col-8">
        <h2><?=$item['nombre']?></h2>
        <p><?=$item['descripcion']?></p>
        <form id="upload-file"  method="post" action="<?=base_url('apis/admin_api/subir_documento')?>" class="ajaxPostForm"  style="display:none" data-function-success="recargar_documento">
        	<input type="file" id="file-child" name="file-child" accept="pdf" />
        	<input type="hidden" name="id_documento" id="id_documento" value="" />
        	<input type="hidden" id="id_estacion" name="id_estacion" value="<?=$id_estacion?>" />
        	<input type="submit">
        </form>
        <?php if(isset($items) && (is_array($items) || is_object($items)) && !empty($items) && (count($items) > 0)) :?>
        <?php foreach($items as $i):?>
        <div class="row m-t-30">
            <div class="col-md-8"><h5><?=$i['nombre']?></h5></div>
            <?php if(!empty($i['url'])):?>
            <div class="col-md-4">
                <a id="doc-file" data-fancybox="" class="btn btn-sm btn-success" href="<?=base_url(DOCUMENTOS_PATH.sha1($id_estacion).'/documentos/'.$i['url'])?>"><i class="fa fa-eye" aria-hidden="true"></i> Visualizar</a>
            </div>
            <?php else: ?>
            <div class="col-md-4">
                <a class="doc-carga btn btn-primary btn-sm" href="#"><i class="fa fa-upload" aria-hidden="true"></i> Carga tu documento</a>
            </div>
            <?php endif;?>
        </div>
        <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <div class="col-3 text-center">
        <a href="http://www.petroaadlab.com/" target="_blank"><img id="doc-video-img" class="img-fluid" src="<?=base_url('assets/img/petrolab.jpg')?>" /></a>
        <br>
        <br>
        <div class="text-center">
             <a class="btn btn-primary" href="https://www.sat.gob.mx">Más información</a>
        </div>
    </div>
</div>