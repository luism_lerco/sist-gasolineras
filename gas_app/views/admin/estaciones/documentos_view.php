<div class="secondary-sidebar light" data-init="secondary-sidebar">
    <div class="scroll-wrapper">
        <!--<ul class="menu-items scroll-content scroll-scrolly_visible">-->
        <ul class="main-menu">
            <?php foreach($dependencias as $index => $d):?>
            <li <?=($index==0)?'class="active"':""?>>
                <a><span class="title"><?=$d['nombre']?></span>
                <span class=" arrow"></span></a>
                <ul class="main-menu no-padding">
                    <?php foreach($d['documentos'] as $doc):?>
                    <li class="">

                        <a class="documento-link" data-id="<?=$doc['id_documento']?>" href="#"><span class="title"><?=$doc['nombre_corto']?></span></a>
                    </li>
                    <?php endforeach; ?>
                    <li class="m-b-50"></li>
                </ul>
                <?php endforeach;?>
            </li>
        </ul>
    </div>
</div>
<div class="inner-content full-height">
    <div id="right-content" class="row justify-content-center ">
    </div>
</div>