<form role="form" method="post" action="<?=$url_form?>" class="ajaxPostFormModal m-b-20 m-t-20" data-function-success="mostrar_exito">
    <div class="row">
        <div class="col-xl-4 col-lg-4 col-12 text-center">
            <div class="form-group">
                <label>Logo</label>
                <input id="logo" name="logo" type="hidden" value="<?= (isset($item) ? $item[0]['logo'] : '') ?>"/>
                <p class="text-center">
                    <a href="#" onclick="$('#file-uploader').data('target', '#img_imagen');$('#file-uploader').data('id', '#imagen');$('#file-uploader').find('input').click();return false;">
                        <img id="img_imagen" class="img-responsive img-thumbnail" src="<?php
                        if (isset($item) && $item[0]['logo'] != '')
                            echo base_url(DOCUMENTOS_PATH .'/'.sha1($estacion['id_estacion']).'/imagenes/'.$item[0]['logo']);
                        else
                            echo 'http://placehold.it/200x200';
                        ?>"/>
                    </a>
                </p>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="rfc">R.F.C</label>
                        <input name="rfc" class="form-control" placeholder="R.F.C." value="<?= (isset($item) ? $item[0]['rfc'] : '') ?>"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="razon_social">Razón Social *</label>
                        <input name="razon_social" class="form-control" placeholder="Razón Social." value="<?= (isset($item) ? $item[0]['razon_social'] : '') ?>"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="nombre">Nombre *</label>
                        <input name="nombre" class="form-control"
                               placeholder="Nombre" value="<?= (isset($item) ? $item[0]['nombre'] : '') ?>"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="no_permiso">No. Permiso de la CRE</label>
                        <input name="no_permiso" class="form-control"
                               placeholder="No. Permiso" value="<?= (isset($item) ? $item[0]['no_permiso'] : '') ?>"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="nombre_representante">Nombre del representante Legal *</label>
                        <input name="nombre_representante" class="form-control"
                               placeholder="Nombre Completo" value="<?= (isset($item) ? $item[0]['nombre_representante'] : '') ?>" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="nombre_encargado">Nombre del Encargado de la Estación de Servicio *</label>
                        <input name="nombre_encargado" class="form-control"
                               placeholder="Nombre Completo" value="<?= (isset($item) ? $item[0]['nombre_encargado'] : '') ?>"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="telefono">Teléfono *</label>
                        <input name="telefono" class="form-control"
                               placeholder="Teléfono" value="<?= (isset($item) ? $item[0]['telefono'] : '') ?>"/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="email">Correo Electrónico *</label>
                        <input name="email" class="form-control"
                               placeholder="Correo Electrónico" value="<?= (isset($item) ? $item[0]['email'] : '') ?>"/>
                    </div>
                </div>
            </div>
            <fieldset>
                <legend>Dirección</legend>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="calle">Calle *</label>
                            <input name="calle" class="form-control"
                                   placeholder="Calle" value="<?= (isset($item) ? $item[0]['calle'] : '') ?>"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="no_ext">No.Ext *</label>
                            <input name="no_ext" class="form-control"
                                   placeholder="No.Ext" value="<?= (isset($item) ? $item[0]['no_ext'] : '') ?>"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="no_int">No. Int</label>
                            <input name="no_int" class="form-control"
                                   placeholder="No.Int" value="<?= (isset($item) ? $item[0]['no_int'] : '') ?>"/>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="cp">C.P. *</label>
                            <input name="cp" class="form-control"
                                   placeholder="C.P." value="<?= (isset($item) ? $item[0]['cp'] : '') ?>"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="colonia">Colonia *</label>
                            <input name="colonia" class="form-control"
                                   placeholder="Colonia" value="<?= (isset($item) ? $item[0]['colonia'] : '') ?>"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="municipio">Municipio *</label>
                            <input name="municipio" class="form-control"
                                   placeholder="Municipio" value="<?= (isset($item) ? $item[0]['municipio'] : '') ?>"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="estado">Estado *</label>
                            <input name="estado" class="form-control"
                                   placeholder="Estado" value="<?= (isset($item) ? $item[0]['estado'] : '') ?>"/>
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="clearfix m-t-10"></div>
            <input type="hidden" name="id" value="<?= (isset($item) ? $item[0]['id_estacion'] : '') ?>"/>
            <button type="submit" class="btn btn-success m-r-5">Guardar</button>
            <a href="<?=base_url('admin/inicio')?>" class="btn btn-default">Cancelar</a>
            <div class="clearfix"></div>
        </div>
    </div>
    </div>
</form>