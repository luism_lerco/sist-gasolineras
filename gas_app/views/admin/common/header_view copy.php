<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title><?= isset($TITLE) ? $TITLE.' | ' : '' ?><?= NOMBRE_SITIO ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/pace/pace-theme-flash.css')?>"/>
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/bootstrap/css/bootstrap.min.css')?>"/>
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/tether/css/tether.min.css')?>"/>
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/font-awesome/css/font-awesome.css')?>"/>
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/jquery-scrollbar/jquery.scrollbar.css')?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/bootstrap-select2/select2.css')?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/switchery/css/switchery.min.css')?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/fancybox/jquery.fancybox.min.css')?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/pages/css/pages-icons.css')?>">
    <link rel="stylesheet" type="text/css" class="main-stylesheet" href="<?=base_url('assets/pages/css/themes/corporate.css')?>" />
    <link rel="stylesheet" type="text/css" class="main-stylesheet" href="<?=base_url('assets/css/style.css')?>" />
    <?=isset($css)?$css:''?>
</head>
<body class="fixed-header menu-pin">
    <div class="page-sidebar" data-pages="sidebar">
        <div class="sidebar-header">
            <img src="<?=base_url('assets/img/logo_white.png')?>" alt="logo" class="brand" data-src="<?=base_url('assets/img/logo_white.png')?>" data-src-retina="<?=base_url('assets/img/logo_white.png')?>" width="78" height="22">
            <div class="sidebar-header-controls">
                <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i></button>
            </div>
        </div>
        <div class="sidebar-menu">
            <ul class="menu-items">
                <!--<li id="liInicio" class="m-t-30">
                    <a href="<?=base_url('admin')?>"><span class="title">Inicio</span></a>
                    <span class="icon-thumbnail "><i class="pg-charts"></i></span>
                </li>-->
                <li id="liEstaciones" class="">
                    <a href="javascript:;">
                        <span class="title">Estaciones</span>
                        <span class="arrow"></span>
                    </a>
                    <span class="icon-thumbnail"><i class="pg-cupboard"></i></span>
                    <ul class="sub-menu">
                        <?php if(isset($this->estaciones) && !empty($this->estaciones) ):?>
                        <?php foreach($this->estaciones as $item):?>
                        <li id="lk-<?=$item['id_estacion']?>">
                            <a>
                                <span class="title"><?=$item['nombre']?></span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li id="lk-<?=$item['id_estacion']?>-info" class="">
                                    <a href="<?=base_url('admin/estaciones/'.$item['id_estacion'].'/informacion-general')?>">Información Gral.</a>
                                </li>
                                <li id="lk-<?=$item['id_estacion']?>-documents" class="">
                                    <a href="<?=base_url('admin/estaciones/'.$item['id_estacion'].'/autoridades')?>">Autoridades</a>
                                </li>
                                <li id="lk-<?=$item['id_estacion']?>-info" class="">
                                    <a href="<?=base_url('admin/estaciones/'.$item['id_estacion'].'/sasisopa')?>">SASISOPA</a>
                                </li>
                                <li id="lk-<?=$item['id_estacion']?>-users" class="<?=base_url('admin/estaciones/'.$item['id_estacion'].'/usuarios')?>">
                                    <a href="#">Usuarios</a>
                                </li>
                            </ul>
                        </li>
                        <?php endforeach;?>
                        <?php endif;?>
                        <li id="lkNueva">
                            <a href="<?=basE_url('admin/estaciones/nueva')?>">Nueva Estación</a>
                            <span class="icon-thumbnail "><i class="pg-plus"></i></span>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
    <!-- START PAGE-CONTAINER -->
    <div class="page-container">
      <!-- START PAGE HEADER WRAPPER -->
      <!-- START HEADER -->
      <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <div class="container-fluid relative">
          <!-- LEFT SIDE -->
          <div class="pull-left full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="header-inner">
              <a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
                <span class="icon-set menu-hambuger"></span>
              </a>
            </div>
            <!-- END ACTION BAR -->

          </div>
          <!-- RIGHT SIDE -->
          <div class="pull-right full-height visible-sm visible-xs">
            <!-- START ACTION BAR -->
            <div class="header-inner">
              <a href="#" class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview" data-toggle-element="#quickview">
                <span class="icon-set menu-hambuger-plus"></span>
              </a>
            </div>
            <!-- END ACTION BAR -->
          </div>
        </div>
        <!-- END MOBILE CONTROLS -->
        <div class="pull-left sm-table hidden-xs hidden-sm">
            <ul class="d-lg-inline-block d-none notification-list no-margin d-lg-inline-block b-grey b-r no-style p-l-30 p-r-20">
                <li class="p-r-10 inline">
                    Inicio <a href="<?=base_url('admin')?>" class="header-icon pg pg-charts"></a>
                </li>
            </ul>
        </div>
        <div class=" pull-right">
          <div class="header-inner">
            <a href="#" class="btn-link icon-set menu-hambuger-plus m-l-20 sm-no-margin hidden-sm hidden-xs" data-toggle="quickview" data-toggle-element="#quickview"></a>
          </div>
        </div>
        <div class="visible-lg visible-md">
                <div class="pull-right p-r-10 p-t-10 fs-16 font-heading">
                    <!-- START User Info-->
                    <div class="pull-left p-r-10 fs-14 font-heading hidden-md-down">
                        <span class="semi-bold"><?=$this->session->userdata('nombre')?></span> <span class="text-master"><?=$this->session->userdata('paterno').' '.$this->session->userdata('materno')?></span>
                    </div>
                    <div class="dropdown pull-right hidden-md-down">
                        <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="thumbnail-wrapper d32 circular inline">
                                <img src="<?=base_url('assets/img/profiles/avatar.jpg')?>" alt="" data-src="<?=base_url('assets/img/profiles/avatar.jpg')?>" data-src-retina="assets/img/profiles/avatar_small2x.jpg" width="32" height="32">
                            </span>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                            <a href="<?=base_url('admin/mi-perfil')?>" class="dropdown-item"><i class="pg-settings_small"></i> Mi Perfil</a>
                            <a href="<?=base_url('admin/ayuda')?>" class="dropdown-item"><i class="pg-signals"></i> Ayuda</a>
                            <a href="<?=base_url('admin/salir')?>" class="clearfix bg-master-lighter dropdown-item">
                                <span class="pull-left">Salir</span>
                                <span class="pull-right"><i class="pg-power"></i></span>
                            </a>
                        </div>
                    </div>
                  <!-- END User Info-->
            </div>
        </div>
        </div>
      <!-- END HEADER -->
      <!-- END PAGE HEADER WRAPPER -->
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper <?=(isset($full_height) && $full_height)?'full-height':''?>">
        <!-- START PAGE CONTENT -->
        <div class="content <?=(isset($full_height) && $full_height)?'full-height':''?>">

          <?php if(!isset($full_height) || !$full_height):?>
          <!-- START JUMBOTRON -->
          <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
              <div class="inner">
              <!-- START BREADCRUMB -->
              <?php if (isset($breadcumb)) { ?>
                  <!-- begin breadcrumb -->
                  <ol class="breadcrumb">
                      <?php
                      for ($i = 0; $i < sizeof($breadcumb); $i++) {
                          if ($i == (sizeof($breadcumb) - 1)) {
                              echo '<li class="breadcrumb-item active">' . $breadcumb[$i]['label'];
                          } else {
                              echo '<li class="breadcrumb-item">';
                              echo '<a href="' . (isset($breadcumb[$i]['url']) ? $breadcumb[$i]['url'] : 'javascript:;') . '">';
                              echo $breadcumb[$i]['label'] . '</a>';
                          }
                          echo '</li>';
                      }
                      ?>
                  </ol>
                  <!-- end breadcrumb -->
              <?php } ?>
              <!-- END BREADCRUMB -->
              </div>
            </div>
          </div>
          <!-- END JUMBOTRON -->
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">
            <!-- BEGIN PlACE PAGE CONTENT HERE -->
          <?php endif?>