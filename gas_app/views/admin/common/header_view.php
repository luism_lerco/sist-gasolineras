<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title><?= isset($TITLE) ? $TITLE.' | ' : '' ?><?= NOMBRE_SITIO ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
    <link rel="apple-touch-icon" sizes="57x57" href="<?=base_url('assets/img/favicon/apple-icon-57x57.png')?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=base_url('assets/img/favicon/apple-icon-60x60.png')?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=base_url('assets/img/favicon/apple-icon-72x72.png')?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url('assets/img/favicon/apple-icon-76x76.png')?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=base_url('assets/img/favicon/apple-icon-114x114.png')?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=base_url('assets/img/favicon/apple-icon-120x120.png')?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=base_url('assets/img/favicon/apple-icon-144x144.png')?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=base_url('assets/img/favicon/apple-icon-152x152.png')?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=base_url('assets/img/favicon/apple-icon-180x180.png')?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?=base_url('assets/img/favicon/android-icon-192x192.png')?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=base_url('assets/img/favicon/favicon-32x32.png')?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?=base_url('assets/img/favicon/favicon-96x96.png')?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url('assets/img/favicon/favicon-16x16.png')?>">
    <link rel="manifest" href="<?=base_url('assets/img/favicon/manifest.json')?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?=base_url('assets/img/favicon/ms-icon-144x144.png')?>">
    <meta name="theme-color" content="#ffffff">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/pace/pace-theme-flash.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/font-awesome/css/font-awesome.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/jquery-scrollbar/jquery.scrollbar.css')?>"  media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/select2/css/select2.min.css')?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/switchery/css/switchery.min.css')?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/fancybox/css/jquery.fancybox.min.css')?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/pages/css/pages-icons.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/pages/css/themes/modern.css')?>" class="main-stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/pages/css/themes/calendar.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/custom.css')?>" />
    <?=isset($css)?$css:''?>
  </head>
  <body class="fixed-header horizontal-menu horizontal-app-menu ">
    <!-- START PAGE-CONTAINER -->
    <div class="header p-r-0 bg-red">
        <div class="header-inner header-md-height">
            <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu text-white" data-toggle="horizontal-menu"></a>
            <div class="">
                <div class="brand inline no-border hidden-xs-down">
                    <a href="<?=base_url('admin/inicio')?>">
                        <img src="<?=base_url('assets/img/logo_white.png')?>" alt="logo" class="brand" data-src="<?=base_url('assets/img/logo_white.png')?>" data-src-retina="<?=base_url('assets/img/logo_white.png')?>" width="78" height="22">
                    </a>
                </div>
                <!-- START NOTIFICATION LIST -->
                <ul class="hidden-md-down notification-list no-margin hidden-sm-down b-grey b-l b-r no-style p-l-30 p-r-20">
                    <li class="p-r-10 inline">
                        <div class="dropdown">
                            <a href="javascript:;" id="notification-center" class="header-icon pg pg-world" data-toggle="dropdown">
                                <span class="bubble"></span>
                            </a>
                            <!-- START Notification Dropdown -->
                            <div class="dropdown-menu notification-toggle" role="menu" aria-labelledby="notification-center">
                              <!-- START Notification -->
                              <div class="notification-panel">
                                <!-- START Notification Body-->
                                <div class="notification-body scrollable">
                                  <!-- START Notification Item-->
                                  <div class="notification-item unread clearfix">
                                    <!-- START Notification Item Right Side-->
                                    <div class="option" data-toggle="tooltip" data-placement="left" title="mark as read">
                                      <a href="#" class="mark"></a>
                                    </div>
                                    <!-- END Notification Item Right Side-->
                                  </div>
                                  <!-- START Notification Body-->
                                  <!-- START Notification Item-->
                                  <div class="notification-item unread clearfix">
                                    <div class="heading">
                                      <a href="#" class="text-danger pull-left">
                                        <i class="fa fa-exclamation-triangle m-r-10"></i>
                                        <span class="bold">Documento vencido</span>
                                      </a>
                                      <span class="pull-right time">Hace 2 mins</span>
                                    </div>
                                    <!-- START Notification Item Right Side-->
                                    <div class="option">
                                      <a href="#" class="mark"></a>
                                    </div>
                                    <!-- END Notification Item Right Side-->
                                  </div>
                                  <!-- END Notification Item-->
                                  <!-- START Notification Item-->
                                  <div class="notification-item unread clearfix">
                                    <div class="heading">
                                      <a href="#" class="text-warning-dark pull-left">
                                        <i class="fa fa-exclamation-triangle m-r-10"></i>
                                        <span class="bold">Documento proximo a vencer</span>
                                      </a>
                                      <span class="pull-right time">Ayer</span>
                                    </div>
                                    <!-- START Notification Item Right Side-->
                                    <div class="option">
                                      <a href="#" class="mark"></a>
                                    </div>
                                    <!-- END Notification Item Right Side-->
                                  </div>
                                  <!-- END Notification Item-->
                                  <!-- START Notification Item-->
                                  <div class="notification-item unread clearfix">
                                    <div class="heading">
                                      <div class="thumbnail-wrapper d24 circular b-white m-r-5 b-a b-white m-t-10 m-r-10">
                                        <img width="30" height="30" data-src-retina="assets/img/profiles/1x.jpg" data-src="assets/img/profiles/1.jpg" alt="" src="assets/img/profiles/1.jpg">
                                      </div>
                                      <a href="#" class="text-complete pull-left">
                                        <span class="bold">Documento agregado</span>
                                      </a>
                                      <span class="pull-right time">10-Oct-2018 14:20</span>
                                    </div>
                                    <!-- START Notification Item Right Side-->
                                    <div class="option" data-toggle="tooltip" data-placement="left" title="mark as read">
                                      <a href="#" class="mark"></a>
                                    </div>
                                    <!-- END Notification Item Right Side-->
                                  </div>
                                  <!-- END Notification Item-->
                                </div>
                                <!-- END Notification Body-->
                                <!-- START Notification Footer-->
                                <div class="notification-footer text-center">
                                  <a href="#" class="">Leer todas las notificaciones</a>
                                  <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="#">
                                    <i class="pg-refresh_new"></i>
                                  </a>
                                </div>
                                <!-- START Notification Footer-->
                              </div>
                              <!-- END Notification -->
                            </div>
                            <!-- END Notification Dropdown -->
                        </div>
                    </li>
                    <li class="p-r-10 inline">
                        <a href="<?=base_url('admin/inicio')?>" class="header-icon pg pg-home"></a>
                    </li>
                </ul>
                <!-- END NOTIFICATIONS LIST -->
            </div>
            <div class="d-flex align-items-center">
            <!-- START User Info-->
                <div class="pull-left p-r-10 fs-14 font-heading hidden-md-down text-white">
                    <span class="semi-bold"><?=$this->session->userdata('nombre')?></span> <span class="text-master"><?=$this->session->userdata('paterno').' '.$this->session->userdata('materno')?></span>
                </div>
                <div class="dropdown pull-right">
                    <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="thumbnail-wrapper d32 circular inline sm-m-r-5">
                        <img src="<?=base_url('assets/img/profiles/avatar.jpg')?>" alt="" data-src="<?=base_url('assets/img/profiles/avatar.jpg')?>" data-src-retina="assets/img/profiles/avatar_small2x.jpg" width="32" height="32" />
                        </span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                        <a class="dropdown-item" href="<?=base_url('admin/mi-perfil')?>"><i class="pg-settings_small"></i> Mi Perfil</a>
                        <a class="dropdown-item" href="<?=base_url('admin/ayuda')?>" class="dropdown-item"><i class="pg-signals"></i> Ayuda</a>
                        <a class="dropdown-item" href="<?=base_url('admin/salir')?>" class="clearfix bg-master-lighter dropdown-item">
                            <span class="pull-left">Salir</span>
                            <span class="pull-right"><i class="pg-power"></i></span>
                        </a>
                    </div>
                </div>
            <!-- END User Info-->
            </div>
        </div>
        <?php if(isset($id_estacion) && $id_estacion > 0):?>
        <div class="bg-white">
            <div class="container">
                <div class="menu-bar header-sm-height" data-pages-init='horizontal-menu' data-hide-extra-li="4">
                    <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-close" data-toggle="horizontal-menu"></a>
                    <ul>
                        <li>
                            <img id="logo_estacion" class="img-fluid" src="<?php
                        if (isset($estacion) && ($estacion['logo'] != '' && !is_null($estacion['logo'])) )
                            echo base_url(DOCUMENTOS_PATH.'/'.sha1($estacion['id_estacion']).'/imagenes/logo_thumbnail.png');
                        else
                            echo 'http://placehold.it/50x50';
                        ?>">
                            <div class="title"><?=$estacion['nombre']?><div class="small"><?=$estacion['rfc']?></div></div>
                            <span class=" arrow"></span>
                            <ul>
                                <?php if(isset($this->estaciones) && !is_null($this->estaciones) && (is_array($this->estaciones) || is_object($this->estaciones)) && !empty($this->estaciones)):?>
                                <?php foreach($this->estaciones AS $e):?>
                                <li>
                                    <a href="<?=base_url('admin/estaciones/'.$e['id_estacion'].'/informacion-general')?>"><?=$e['nombre']?>
                                </li>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                <li><a href="<?=base_url('admin/estaciones/nueva')?>">Agregar Estación</a></li>
                            </ul>
                        </li>
                        <li id="liGeneral">
                            <a href="<?=base_url('admin/estaciones/'.$id_estacion.'/informacion-general')?>"><span class="title">Información Gral.</span></a>
                        </li>
                        <li id="liAutoridades">
                            <a href="<?=base_url('admin/estaciones/'.$id_estacion.'/autoridades')?>"><span class="title">Autoridades</span></a>
                        </li>
                        <li id="liSasisopa">
                            <a href="<?=base_url('admin/estaciones/'.$id_estacion.'/sasisopa')?>"><span class="title">SASISOPA</span></a>
                        </li>
                        <li id="liUsuarios">
                            <a href="<?=base_url('admin/estaciones/'.$id_estacion.'/usuarios')?>"><span class="title">Usuarios</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <?php endif;?>
    </div>
    <div class="page-container">
        <!-- START PAGE CONTENT WRAPPER -->
        <div class="page-content-wrapper <?=(isset($full_height) && $full_height)?'full-height':''?>">
            <!-- START PAGE CONTENT -->
            <?php if(isset($full_height) && $full_height):?>
            <div class="container full-height no-padding">
            <?php else: ?>
            <div class="content">
            <?php endif;?>
            <div class="content">
                <?php if (isset($breadcumb)): ?>
                <div class="bg-white">
                    <div class="container">
                        <!-- begin breadcrumb -->
                        <ol class="breadcrumb breadcrumb-alt">
                        <?php
                            for ($i = 0; $i < sizeof($breadcumb); $i++) {
                              if ($i == (sizeof($breadcumb) - 1)) {
                                  echo '<li class="breadcrumb-item active">' . $breadcumb[$i]['label'];
                              } else {
                                  echo '<li class="breadcrumb-item">';
                                  echo '<a href="' . (isset($breadcumb[$i]['url']) ? $breadcumb[$i]['url'] : 'javascript:;') . '">';
                                  echo $breadcumb[$i]['label'] . '</a>';
                              }
                              echo '</li>';
                          }
                          ?>
                        </ol>
                        <!-- end breadcrumb -->
                    </div>
                </div>
                <div class=" container container-fixed-lg">
                <?php endif; ?>
                <!-- START CONTAINER FLUID -->
                <?php if(isset($full_width) && !$full_width):?>
                <div class=" container <?=(isset($full_height) && $full_height)?'full-height no-padding':'container-fixed-lg'?>">
                <?php endif; ?>
                <!-- BEGIN PlACE PAGE CONTENT HERE -->