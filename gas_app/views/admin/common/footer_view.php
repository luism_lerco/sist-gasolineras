                </div>
            </div>
            <?php if(!isset($full_height) || !$full_height):?>
            <div class="container-fluid container-fixed-lg footer">
                <div class="copyright sm-text-center">
                <p class="small no-margin pull-left sm-pull-reset">
                  <span class="hint-text">Copyright © <?=date('Y')?></span>
                  <span class="hint-text">Todos los derechos reservados.</span>
                  <span class="sm-block">
                    <a data-morphing href="#" data-src="#terminos" class="m-l-10 m-r-10">Terminos de Uso</a> |
                    <a data-morphing href="#" data-src="#politicas" class="m-l-10">Políticas de Privacidad</a>
                  </span>
                </p>
                <p class="small no-margin pull-right sm-pull-reset">
                  <span class="hint-text">Desarrollado por</span>
                  <a href="https://www.lerco.mx">Lerco </a>
                </p>
                <div class="clearfix"></div>
              </div>
            </div>
            <?php endif;?>
        </div>
        <?php $CI = & get_instance(); ?>
        <div id="terminos"><?=$CI->load->view('terminos_view', '', true)?></div>
        <div id="politicas"><?=$CI->load->view('politicas_view', '', true)?></div>
    </div>
    <script type="text/javascript" src="<?=base_url('assets/plugins/pace/pace.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery/jquery-1.11.1.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/modernizr.custom.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-ui/jquery-ui.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/tether/js/tether.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/bootstrap/js/bootstrap.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery/jquery-easy.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-unveil/jquery.unveil.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-bez/jquery.bez.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-ios-list/jquery.ioslist.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/imagesloaded/imagesloaded.pkgd.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-actual/jquery.actual.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/fancybox/js/jquery.fancybox.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/sweetalert/js/sweetalert2.all.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/pages/js/pages.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/interactjs/interact.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/classie/classie.js')?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            RootJS = '<?= base_url() ?>';
            Result = new Result();
            <?php
            if (isset($link_active) && (is_array($link_active) || is_object($link_active) ) ) {

                foreach ($link_active as $lnk) {
                    ?>
                    if($("#<?= $lnk ?>").parent().hasClass('sub-menu')){
                        $("#<?= $lnk ?>").addClass('open active');
                    }else{
                        $("#<?= $lnk ?>").addClass('active');
                    }
                    <?php
                }
            }
            ?>
            $('li.active').has('ul.submenu').addClass('open');
      });
    </script>
    <script type="text/javascript" src="<?=base_url('assets/js/functions.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/js/scripts.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/js/result.js') ?>"></script>
    <?=isset($js)?$js:''?>
</body>
</html>