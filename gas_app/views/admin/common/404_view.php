<div class="error-container text-center">
    <h1 class="error-number">404</h1>
    <h2 class="semi-bold">Lo sentimos pero no pudimos encontrar esta página.</h2>
    <p class="p-b-10">Esta página que buscas no existe</p>
</div>