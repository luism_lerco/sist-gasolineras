<div class="social-wrapper">
    <div class="social " data-pages="social">
        <div class="jumbotron" data-social="cover" data-pages="parallax" data-scroll-element=".page-container">
            <div class="cover-photo" style="background-image: url('<?=base_url("assets/img/banner_inicio.jpg")?>'); transform: translateY(20.1px);">
            </div>
        </div>
    </div>
</div>

<!--<div id="carousel-inicio" class="carousel slide">
    <ol class="carousel-indicators">
        <li data-target="#carousel-inicio" data-slide-to="0" class="active" contenteditable="false"></li>
        <li data-target="#carousel-inicio" data-slide-to="1" class="" contenteditable="false"></li>
        <li data-target="#carousel-inicio" data-slide-to="2" class="" contenteditable="false"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item" style="">
            <img src="<?=base_url('assets/img/slider/slider1.jpg')?>" alt="" class="">
        </div>
        <div class="item active">
            <img src="<?=base_url('assets/img/slider/slider2.jpg')?>" alt="" class="">
        </div>
        <div class="item" style="">
            <img src="<?=base_url('assets/img/slider/slider3.jpg')?>" alt="" class="">
        </div>
    </div>

    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>

    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>-->

<div class=" container container-fixed-lg">

<div class="card card-transparent">
    <div class="card-header">
        <div class="card-title">
            <div class="row">
                <div class="col-md-8">
                    <h2>Estaciones</h2>
                </div>
                <div class="col-md-4">
                    <a id="btn-add" href="<?=base_url('admin/estaciones/nueva')?>" title="Agregar Estación" class="block m-t-15"><img src="<?=base_url('assets/img/plus.svg')?>"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
    <?php $count =0;?>
    <?php $chunk = array_chunk($this->estaciones, 2);?>
    <?php foreach($chunk as $i => $c):?>
        <div class="row m-t-20">
            <?php foreach($c as $k => $e):?>
            <div class="col-lg-6">
                <div class="card card-borderless">
                    <ul class="nav nav-tabs nav-tabs-simple" role="tablist" data-init-reponsive-tabs="dropdownfx">
                        <li class="nav-item">
                            <a class="active" data-toggle="tab" role="tab" data-target="#general-<?=$i?>-<?=$k?>" href="#">General</a>
                        </li>
                        <li class="nav-item">
                            <a href="#" data-toggle="tab" role="tab" data-target="#calendario-<?=$i?>-<?=$k?>">Calendario</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="general-<?=$i?>-<?=$k?>">
                            <div class="card card-transparent">
                                <div class="card-header">
                                    <div class="card-title"><h5><?=$e['nombre']?></h5></div>
                                </div>
                                <div class="row">
                                    <div class="col-8">
                                        <p class="font-montserrat p-l-20">Etapa: <span class="bold">Diseño</span></p>
                                        <p class="font-montserrat p-l-20 bold">Mis Documentos</span></p>
                                        <div class="p-l-20 p-r-20 p-t-10 p-b-10 ">
                                            <a href="<?=base_url('admin/estaciones/'.$e['id_estacion'].'/autoridades')?>" class="btn btn-default btn-rounded">Detalle de documentos</a>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div id="estacion_<?=$e['id_estacion']?>"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="calendario-<?=$i?>-<?=$k?>">
                            <div id="calendario-<?=$count?>" class="full-height"></div>
                        </div>
                    </div>
                </div>
            </div>
            <?php $count++?>
        <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
    </div>
</div>
