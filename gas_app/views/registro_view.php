<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/favicon.ico">
	<title>Registro</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png" />
	<link rel="icon" type="image/png" href="assets/img/favicon.png" />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
	<link rel="stylesheet" href="<?=base_url('assets/plugins/bootstrap/css/bootstrap.min.css')?>" />
	<link rel="stylesheet" href="<?=base_url('assets/plugins/bootstrap-wizard/bootstrap-wizard.css')?>" />

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="<?=base_url('assets/css/register.css')?>" />
</head>

<body>
    <style>
        .btn-purple{
            background-color: #9c27b0 !important;
        }
    </style>
	<div class="image-container set-full-height" style="background-image: url('<?=base_url('assets/img/register/wizard-profile.jpg')?>')">
	    <!--   Creative Tim Branding   -->
	    <a href="<?=base_url()?>">
	         <div class="logo-container">
	            <div class="logo">
	                <img src="<?=base_url('assets/img/logo_white.png')?>">
	            </div>
	        </div>
	    </a>
	    <!--   Big container   -->
	    <div class="container">
	        <div class="row justify-content-center">
		        <div class="col-sm-8">
		            <!--      Wizard container        -->
		            <div class="wizard-container">
		                <div class="card wizard-card" data-color="purple" id="wizardProfile">
		                    <form action="" method="">
		                    	<div class="wizard-header">
		                        	<h3 class="wizard-title">
		                        	   Registro de Usuario
		                        	</h3>
		                    	</div>
								<div class="wizard-navigation">
									<ul>
			                            <li><a href="#about" data-toggle="tab">Básica</a></li>
			                            <li><a href="#account" data-toggle="tab">Empresa</a></li>
			                            <li><a href="#address" data-toggle="tab">Estación</a></li>
			                        </ul>
								</div>

		                        <div class="tab-content">
		                            <div class="tab-pane" id="about">
                                        <h4 class="info-text"> Empecemos con información básica</h4>
                                        <div class="row justify-content-center">
		                                	<div class="col-sm-12">
												<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">face</i>
													</span>
													<div class="form-group label-floating">
			                                          <label class="control-label">Nombre <small>(obligatorio)</small></label>
			                                          <input name="firstname" type="text" class="form-control">
			                                        </div>
												</div>
		                                	</div>
		                                </div>
                                        <div class="row">
                                            <div class="col-sm-6">
												<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">record_voice_over</i>
													</span>
													<div class="form-group label-floating">
													  <label class="control-label">Apellido Paterno <small>(obligatorio)</small></label>
													  <input name="lastname" type="text" class="form-control">
													</div>
												</div>
                                            </div>
                                            <div class="col-sm-6">
												<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">record_voice_over</i>
													</span>
													<div class="form-group label-floating">
													  <label class="control-label">Apellido Materno <small>(obligatorio)</small></label>
													  <input name="lastname" type="text" class="form-control">
													</div>
												</div>
								            </div>
                                        </div>
                                        <div class="row justify-content-center">
		                                	<div class="col-sm-6">
												<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">email</i>
													</span>
													<div class="form-group label-floating">
			                                            <label class="control-label">Email <small>(obligatorio)</small></label>
			                                            <input name="email" type="email" class="form-control">
			                                        </div>
												</div>
		                                	</div>
		                                	<div class="col-sm-6">
												<div class="input-group">
													<span class="input-group-addon">
														<i class="material-icons">phone</i>
													</span>
													<div class="form-group label-floating">
			                                            <label class="control-label">Teléfono <small>(obligatorio)</small></label>
			                                            <input name="email" type="email" class="form-control">
			                                        </div>
												</div>
		                                	</div>
		                            	</div>
		                            </div>
		                            <div class="tab-pane" id="account">
		                                <h4 class="info-text"> What are you doing? (checkboxes) </h4>
		                                <div class="row justify-content-center">
		                                    <div class="col-sm-10">
		                                        <div class="col-sm-4">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="jobb" value="Design">
		                                                <div class="icon">
		                                                    <i class="fa fa-pencil"></i>
		                                                </div>
		                                                <h6>Design</h6>
		                                            </div>
		                                        </div>
		                                        <div class="col-sm-4">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="jobb" value="Code">
		                                                <div class="icon">
		                                                    <i class="fa fa-terminal"></i>
		                                                </div>
		                                                <h6>Code</h6>
		                                            </div>
		                                        </div>
		                                        <div class="col-sm-4">
		                                            <div class="choice" data-toggle="wizard-checkbox">
		                                                <input type="checkbox" name="jobb" value="Develop">
		                                                <div class="icon">
		                                                    <i class="fa fa-laptop"></i>
		                                                </div>
		                                                <h6>Develop</h6>
		                                            </div>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="tab-pane" id="address">
		                                <div class="row">
		                                    <div class="col-sm-12">
		                                        <h4 class="info-text"> Are you living in a nice area? </h4>
		                                    </div>
		                                    <div class="col-sm-7 col-sm-offset-1">
	                                        	<div class="form-group label-floating">
	                                        		<label class="control-label">Street Name</label>
	                                    			<input type="text" class="form-control">
	                                        	</div>
		                                    </div>
		                                    <div class="col-sm-3">
		                                        <div class="form-group label-floating">
		                                            <label class="control-label">Street Number</label>
		                                            <input type="text" class="form-control">
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-5 col-sm-offset-1">
		                                        <div class="form-group label-floating">
		                                            <label class="control-label">City</label>
		                                            <input type="text" class="form-control">
		                                        </div>
		                                    </div>
		                                    <div class="col-sm-5">
		                                        <div class="form-group label-floating">
		                                            <label class="control-label">Country</label>
	                                            	<select name="country" class="form-control">
														<option disabled="" selected=""></option>
	                                                	<option value="Afghanistan"> Afghanistan </option>
	                                                	<option value="Albania"> Albania </option>
	                                                	<option value="Algeria"> Algeria </option>
	                                                	<option value="American Samoa"> American Samoa </option>
	                                                	<option value="Andorra"> Andorra </option>
	                                                	<option value="Angola"> Angola </option>
	                                                	<option value="Anguilla"> Anguilla </option>
	                                                	<option value="Antarctica"> Antarctica </option>
	                                                	<option value="...">...</option>
	                                            	</select>
		                                        </div>
		                                    </div>
		                                </div>
		                            </div>
		                        </div>
		                        <div class="wizard-footer">
		                            <div class="pull-right">
		                                <input type='button' class='btn btn-next btn-purple btn-fill btn-success btn-wd' name='next' value='Siguiente' />
		                                <input type='button' class='btn btn-finish btn-purple btn-fill btn-success btn-wd' name='finish' value='Finalizar' />
		                            </div>

		                            <div class="pull-left">
    		                            <a class="btn btn-default btn-fill btn-wd" href="<?=base_url('admin/login')?>">Regresar</a>
		                                <input type='button' class='btn btn-previous btn-purple btn-fill btn-default btn-wd' name='previous' value='Anterior' />
		                            </div>
		                            <div class="clearfix"></div>
		                        </div>
		                    </form>
		                </div>
		            </div> <!-- wizard container -->
		        </div>
	        </div><!-- end row -->
	    </div> <!--  big container -->
	</div>

</body>
	<!--   Core JS Files   -->
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery/jquery-1.11.1.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-actual/jquery.actual.min.js')?>""></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/tether/js/tether.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/plugins/bootstrap/js/bootstrap.min.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/plugins/bootstrap/js/jquery.bootstrap.js')?>"></script>
	<script type="text/javascript" src="<?=base_url('assets/plugins/bootstrap-wizard/bootstrap-wizard.js')?>"></script>

    <!--  More information about jquery.validate here: http://jqueryvalidation.org/	 -->
	<script type="text/javascript" src="<?=base_url('assets/plugins/jquery-validation/js/jquery.validate.min.js')?>"></script>

</html>
