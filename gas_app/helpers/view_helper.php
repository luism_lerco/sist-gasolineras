<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('dashboard_render'))
{
    function dashboard_render($view = NULL, $data = [], $hdata = [], $fdata = [] )
    {
        date_default_timezone_set('America/Mexico_City');
        $CI = & get_instance();
        $CI->load->view('admin/common/header_view', $hdata);
        if(!is_null($view)){ $CI->load->view($view, $data); }
        $CI->load->view('admin/common/footer_view', $fdata);
    }
}

if (!function_exists("load_view"))
{
    function load_view($viewName, $data = array())
    {
        $CI = & get_instance();
        return $CI->load->view($viewName, $data, true);
    }
}
