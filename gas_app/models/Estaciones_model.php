<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Estaciones_model extends MY_model {

    protected $table = 'estaciones';
    protected $primary_key = 'id_estacion';

    public function __construct() {
        parent::__construct();
    }
}
?>