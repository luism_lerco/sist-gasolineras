<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Estaciones_has_documentos_model extends MY_model {

    protected $table = 'estaciones_has_documentos';
    //protected $primary_key = 'id_grupo';

    public function __construct() {
        parent::__construct();
    }

    public function get($id = NULL, $order_by = NULL, $like = null, $desactivarComillas = null, $limit = null) {
        if (is_numeric($id)) {
            $this->db->where($this->primary_key, $id);
        }
        if (is_array($id)) {
            foreach ($id as $_key => $_value) {
                $this->db->where($_key, $_value, $desactivarComillas);
            }
        }
        if(is_string($order_by)){
            $this->db->order_by($order_by);
        }
        if (is_array($order_by)) {
            foreach ($order_by as $_value) {
                $this->db->order_by($_value);
            }
        }

        if($like != null){
        	if(is_array($like)){
        		foreach ($like as $k => $v){
        			$this->db->like($k, $v);
        		}
        	}
        }

        if($limit != null){
            $this->db->limit($limit);
        }

        $this->db->select('documentos.nombre, documentos.descripcion, documentos.se_archiva, documentos.condicional, documentos.activo AS doc_activo, documentos.eliminado AS doc_eliminado, id_dependencia, documentos.url_mas_info AS url_mas_info, id_etapa, tiempo_vigencia, IFNULL(estaciones_has_documentos.url, "") AS url, IFNULL(estaciones_has_documentos.fecha_documento, "") AS fecha_documento, IFNULL(estaciones_has_documentos.activo, "") AS activo , IFNULL(estaciones_has_documentos.eliminado,"") AS eliminado, documentos.id_documento');
        $this->db->from('documentos');
        $this->db->join($this->table, $this->table.'.id_documento=documentos.id_documento', 'LEFT');
        $q = $this->db->get();
        return $q->result_array();
    }
}
?>