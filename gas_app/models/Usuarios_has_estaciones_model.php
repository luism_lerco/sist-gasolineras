<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_has_estaciones_model extends MY_model {

    protected $table = 'usuarios_has_estaciones';
    //protected $primary_key = 'id_grupo';

    public function __construct() {
        parent::__construct();
    }

    public function get($id = NULL, $order_by = NULL, $like = null, $desactivarComillas = null, $limit = null) {
        if (is_numeric($id)) {
            $this->db->where($this->primary_key, $id);
        }
        if (is_array($id)) {
            foreach ($id as $_key => $_value) {
                $this->db->where($_key, $_value, $desactivarComillas);
            }
        }
        if(is_string($order_by)){
            $this->db->order_by($order_by);
        }
        if (is_array($order_by)) {
            foreach ($order_by as $_value) {
                $this->db->order_by($_value);
            }
        }

        if($like != null){
        	if(is_array($like)){
        		foreach ($like as $k => $v){
        			$this->db->like($k, $v);
        		}
        	}
        }

        if($limit != null){
            $this->db->limit($limit);
        }

        $this->db->select($this->table.'.*, grupos.nombre, grupos.descripcion');
        $this->db->from($this->table);
        $this->db->join('grupos', $this->table.'.id_grupo= grupos.id_grupo');

        $q = $this->db->get();
        return $q->result_array();
    }
}
?>