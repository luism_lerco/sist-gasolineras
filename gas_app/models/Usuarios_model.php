<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_model extends MY_model {

    protected $table = 'usuarios';
    protected $primary_key = 'id_usuario';

    public function __construct() {
        parent::__construct();
    }
}
?>