<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Documentos_model extends MY_model {

    protected $table = 'documentos';
    protected $primary_key = 'id_documento';

    public function __construct() {
        parent::__construct();
    }
}
?>