<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dependencias_model extends MY_model {

    protected $table = 'dependencias';
    protected $primary_key = 'id_dependencia';

    public function __construct() {
        parent::__construct();
    }
}
?>