-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 01-04-2019 a las 17:06:37
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `meximed_sigp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dependencias`
--

CREATE TABLE `dependencias` (
  `id_dependencia` int(10) UNSIGNED NOT NULL,
  `clave` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` text,
  `activo` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0',
  `orden` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `dependencias`
--

INSERT INTO `dependencias` (`id_dependencia`, `clave`, `nombre`, `descripcion`, `activo`, `eliminado`, `orden`) VALUES
(1, 'STPS', 'STPS', NULL, 1, 0, 8),
(2, 'PROFECO', 'PROFECO', NULL, 1, 0, 6),
(3, 'PROTECCIÓN CIVIL', 'PROTECCIÓN CIVIL', NULL, 1, 0, 7),
(4, 'AYUNTAMIENTO MUNICIPAL', 'AYUNTAMIENTO MUNICIPAL', NULL, 1, 0, 2),
(5, 'CONAGUA', 'CONAGUA', NULL, 1, 0, 10),
(6, 'SAT', 'SAT', NULL, 1, 0, 1),
(7, 'SECRETARÍA DE FINANZAS', 'SECRETARÍA DE FINANZAS', NULL, 1, 0, 9),
(8, 'CRE', 'CRE', NULL, 1, 0, 5),
(9, 'ASEA', 'ASEA', NULL, 1, 0, 3),
(10, 'SENER', 'SENER', NULL, 1, 0, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documentos`
--

CREATE TABLE `documentos` (
  `id_documento` int(10) UNSIGNED NOT NULL,
  `id_padre` int(10) UNSIGNED DEFAULT NULL,
  `id_dependencia` int(10) UNSIGNED NOT NULL,
  `id_etapa` int(10) UNSIGNED NOT NULL,
  `nombre_corto` varchar(255) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` text,
  `tiempo_vigencia` varchar(255) DEFAULT NULL,
  `banner_proveedor` varchar(255) NOT NULL,
  `url_proveedor` varchar(255) NOT NULL,
  `se_archiva` tinyint(1) NOT NULL DEFAULT '0',
  `condicional` tinyint(1) NOT NULL DEFAULT '0',
  `es_bitacora` tinyint(1) NOT NULL DEFAULT '0',
  `url_mas_info` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `eliminado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `documentos`
--

INSERT INTO `documentos` (`id_documento`, `id_padre`, `id_dependencia`, `id_etapa`, `nombre_corto`, `nombre`, `descripcion`, `tiempo_vigencia`, `banner_proveedor`, `url_proveedor`, `se_archiva`, `condicional`, `es_bitacora`, `url_mas_info`, `activo`, `eliminado`) VALUES
(1, NULL, 1, 3, 'NOM-001-STPS-2008', 'NORMA Oficial Mexicana NOM-001-STPS-2008, Edificios, locales, instalaciones y áreas en los centros de trabajoCondiciones\r\nde seguridad', 'NORMA Oficial Mexicana NOM-001-STPS-2008, Edificios, locales, instalaciones y áreas en los centros de trabajoCondiciones\r\nde seguridad.', NULL, '', '', 0, 0, 0, 'http://www.stps.gob.mx/bp/secciones/dgsst/normatividad/normas/Nom-001.pdf', 1, 0),
(2, NULL, 1, 3, 'NOM-002-STPS-2010', 'NORMA Oficial Mexicana NOM-002-STPS-2010, Condiciones de seguridad-Prevención y protección contra\r\nincendios en los centros de trabajo', 'NORMA Oficial Mexicana NOM-002-STPS-2010, Condiciones de seguridad-Prevención y protección contra\r\nincendios en los centros de trabajo.', NULL, '', '', 0, 0, 0, 'http://www.stps.gob.mx/bp/secciones/dgsst/normatividad/normas/Nom-002.pdf', 1, 0),
(3, NULL, 1, 3, 'NOM-004-STPS-1999', 'NORMA Oficial Mexicana NOM-004-STPS-1999, Sistemas de protección y dispositivos de seguridad en la maquinaria y equipo que se utilicen en los centros de trabajo. ', 'NORMA Oficial Mexicana NOM-004-STPS-1999, Sistemas de protección y dispositivos de seguridad en la maquinaria y equipo que se utilicen en los centros de trabajo.', NULL, '', '', 0, 0, 0, 'http://www.stps.gob.mx/bp/secciones/dgsst/publicaciones/guias/Guia_004.pdf', 1, 0),
(4, NULL, 1, 3, 'NOM-005-STPS-1998', '\"NORMA Oficial Mexicana NOM-005-STPS-1998, Relativa a las condiciones de seguridad e higiene en los centros\r\nde trabajo para el manejo, transporte y almacenamiento de sustancias químicas peligrosas. \"', '\"NORMA Oficial Mexicana NOM-005-STPS-1998, Relativa a las condiciones de seguridad e higiene en los centros\r\nde trabajo para el manejo, transporte y almacenamiento de sustancias químicas peligrosas. \"', NULL, '', '', 0, 0, 0, 'http://www.stps.gob.mx/bp/secciones/dgsst/normatividad/normas/Nom-005.pdf', 1, 0),
(5, NULL, 1, 3, 'NOM-009-STPS-2011', 'NORMA Oficial Mexicana NOM-009-STPS-2011, Condiciones de seguridad para realizar trabajos en altura.', 'NORMA Oficial Mexicana NOM-009-STPS-2011, Condiciones de seguridad para realizar trabajos en altura.', NULL, '', '', 0, 0, 0, 'http://asinom.stps.gob.mx:8145/upload/nom/35.pdf', 1, 0),
(6, NULL, 1, 3, 'NOM-011-STPS-2001', 'NORMA Oficial Mexicana  NOM-011-STPS-2001\r\nCONDICIONES DE SEGURIDAD E HIGIENE EN LOS\r\nCENTROS DE TRABAJO DONDE SE GENERE RUIDO', '\"NORMA Oficial Mexicana  NOM-011-STPS-2001\r\nCONDICIONES DE SEGURIDAD E HIGIENE EN LOS\r\nCENTROS DE TRABAJO DONDE SE GENERE RUIDO\"', NULL, '', '', 0, 0, 0, 'http://www.dof.gob.mx/nota_detalle.php?codigo=734536&fecha=17/04/2002', 1, 0),
(7, NULL, 2, 3, 'Certificado de verificación de dispensarios', 'Certificado de verificación de dispensarios', NULL, '+6 months', '', '', 0, 0, 0, NULL, 1, 0),
(8, NULL, 2, 3, 'Certificado de verificación de jarra patrón', 'Certificado de verificación de jarra patrón', NULL, '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(9, NULL, 2, 3, 'Verificación de sistemas electrónicos', 'Verificación de sistemas electrónicos', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(10, NULL, 2, 3, 'Bitácora de incidencias', 'Bitácora de incidencias', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(11, NULL, 3, 3, 'Acuse de autorización de programa de protección civil', 'Acuse de autorización de programa de protección civil', NULL, '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(12, NULL, 3, 3, 'Constancia de medidas preventivas C.I', 'Constancia de medidas preventivas C.I', 'Constancia que se otorga a nivel municipal cuando el establecimiento cuenta con los elementos necesarios de defensa contra incendios.', '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(13, NULL, 3, 3, 'Póliza de seguros de responsabilidad civil', 'Póliza de seguros de responsabilidad civil', 'Seguro el cual ampara los daños a terceros ocasionados por sus actividades empresariales, así como por el uso de los inmuebles utilizados para la operación de la estación de servicio.', '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(14, NULL, 3, 3, 'Dictamen expedida por una UV ', 'Dictamen expedida por una UV ', NULL, '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(15, NULL, 3, 3, 'Dictamen de instalaciones eléctricas', 'Dictamen de instalaciones electricas', 'Dictamen emitido por una unidad de verificación acreditada y aprobada con el objetivo de asegurar que las instalaciones eléctricas cumplan con las condiciones adecuadas de seguridad.', '+5 years', '', '', 0, 0, 0, NULL, 1, 0),
(16, NULL, 3, 3, 'Dictamen estructural', 'Dictamen estructural', 'El dictamen estructural es una evaluación ocular de la seguridad estructural de las construcciones existentes, el cual es elaborado por un director Responsable de Obra acreditado como perito a nivel Municipal.', '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(17, NULL, 3, 3, 'Recarga de Extintores', 'Recarga de Extintores', 'Las recargas y mantenimientos son elaborados por una empresa que cuente con un dictamen de cumplimiento de la Norma Oficial Mexicana NOM-154-SCFI-2005, a fin de garantiza su correcto funcionamiento durante le combate de fuegos incipientes de acuerdo a su diseño.', '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(18, NULL, 3, 3, 'Programa mantenimiento a instalaciones eléctricas, hidráulicas, etc', 'Programa mantenimiento a instalaciones eléctricas, hidráulicas, etc', 'Actividades programadas para evitar posibles daños en los equipos, dispositivos, maquinaria e instalaciones de la estación de servicio.', '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(19, NULL, 3, 3, 'Constancia de capacitación medidas C.I, primeros a...', 'Constancia de capacitación medidas C.I, primeros auxilios', 'Desarrollar los conocimientos en materia de prevencion y combate de incendios conociendo el comportamiento del fuego en base a los materiales que se queman, asi como los distintos metodos de extincion de incendios, conociendo los dispositivos que esten a su alcance y aprendiendo a usarlos de forma adecuada.', '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(20, NULL, 4, 3, 'Licencia de uso de suelo', 'Licencia de uso de suelo', NULL, '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(21, NULL, 4, 3, 'Licencia de construcción', 'Licencia de construcción', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(22, NULL, 4, 3, 'Alineamiento y numero oficial', 'Alineamiento y numero oficial', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(23, NULL, 4, 3, 'Liciencia de derribo de árboles', 'Liciencia de derribo de árboles', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(24, NULL, 4, 3, 'Constancia de terminación de obra', 'Constancia de terminación de obra', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(25, NULL, 5, 3, 'Formato de solicitud de servicios', 'Formato de solicitud de servicios', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(26, NULL, 5, 3, 'INE', 'INE', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(27, NULL, 5, 3, 'Croquis de localización del predio donde se genera las aguas residuales', 'Croquis de localización del predio donde se genera las aguas residuales', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(28, NULL, 5, 3, 'Uso del agua que da origen a la descarga', 'Uso del agua que da origen a la descarga', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(29, NULL, 5, 3, 'Descripción de la procedencia de la descarga', 'Descripción de la procedencia de la descarga', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(30, NULL, 5, 3, 'Número del titulo de conseción', 'Número del titulo de conseción', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(31, NULL, 5, 3, 'Descripción del proyecto de las obras de descarga', 'Descripción del proyecto de las obras de descarga', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(32, NULL, 5, 3, 'Memoria técnica que fundamente cómo el solicitante...', 'Memoria técnica que fundamente cómo el solicitante cumplirá las normas', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(33, NULL, 5, 3, 'Características fisico-químicas-bacteriológicas ', 'Caractéristicas fisico-químicas-bacteriologicas ', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(34, NULL, 5, 3, 'Medidas de reuso del agua en su caso', 'Medidas de reuso del agua en su caso', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(35, NULL, 6, 3, 'Inscripción al RFC', 'Inscripción al RFC', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(36, NULL, 6, 3, 'Obtención del certificado de efirma', 'Obtención del certificado de efirma', 'La Firma Electrónica Avanzada es el conjunto de datos y caracteres que permite la identificación del firmante, que ha sido creada por medios electrónicos bajo su exclusivo control, de manera que está vinculada únicamente al mismo y a los datos a los que se refiere, lo que permite que sea detectable cualquier modificación ulterior de éstos, la cual produce los mismos efectos jurídicos que la firma autógrafa. El Certificado Digital es el mensaje de datos o registro que confirme el vínculo entre un firmante y la clave privada.', '+4 years', '', '', 0, 0, 0, 'https://www.sat.gob.mx/tramites/17074/obten-el-certificado-de-e.firma-para-tu-empresa-(antes-firma-electronica)', 1, 0),
(37, NULL, 6, 3, 'Obtención del certificado de sellos digital', 'Obtención del certificado de sellos digital', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(38, NULL, 6, 3, 'Obtención del certificado de sellos digital', 'Obtención del certificado de sellos digital', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(39, NULL, 7, 3, 'Inscripción ante la oficina estatal recaudadora', 'Inscripción ante la oficna estatal recaudadora', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(40, NULL, 7, 3, 'Determinación y pago mensual del impuesto', 'Determinación y pago mensual del impuesto', NULL, '+1 month', '', '', 0, 0, 0, NULL, 1, 0),
(41, NULL, 7, 3, 'Declaración relativa a proveedores y prestadores de servicios', 'Declaración relativa a proveedores y prestadores de servicios', NULL, '+1 month', '', '', 0, 0, 0, NULL, 1, 0),
(42, NULL, 8, 3, 'Estructura de capital', 'Estructura de capital', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(43, NULL, 8, 3, 'Pago de supervisión anual de servicios', 'Pago de supervisión anual de servicios', NULL, '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(44, NULL, 8, 3, 'Información estadística trimestral', 'Información estadística trimestral', NULL, '+3 months', '', '', 0, 0, 0, NULL, 1, 0),
(45, NULL, 8, 3, 'Póliza anual de seguro vigente', 'Póliza anual de seguro vigente', NULL, '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(46, NULL, 8, 3, 'Facturas de procedencia de productos', 'Facturas de procedencia de productos', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(47, NULL, 8, 3, 'Reporte de quejas', 'Reporte de quejas', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(48, NULL, 8, 3, 'Reporte de incidencias/ emergencias', 'Reporte de incidencias/ emergencias', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(49, NULL, 8, 3, 'Pruebas de laboratorio semestrales', 'Pruebas de laboratorio semestrales', NULL, '+6 months', '', '', 0, 0, 0, NULL, 1, 0),
(50, NULL, 8, 3, 'Dictamen NOM-016-CRE-2016', 'Dictamen NOM-016-CRE-2016', NULL, '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(51, NULL, 8, 3, 'Certificado del SGM', 'Certificado del SGM', NULL, '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(52, NULL, 8, 3, 'Registro en SIRETRAC', 'Registro en SIRETRAC', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(53, NULL, 9, 3, 'Evaluación del impacto ambiental', 'Evaluación del impacto ambiental', 'La evaluación de impacto ambiental es un instrumento de la política ambiental, cuyo objetivo es prevenir, mitigar y restaurar los daños al ambiente así como la regulación de obras o actividades para evitar o reducir sus efectos negativos en el ambiente y en la salud humana. A través de este instrumento se plantean opciones de desarrollo que sean compatibles con la preservación del ambiente y manejo de los recursos naturales.', NULL, '', '', 0, 0, 0, 'http://www.semarnat.gob.mx/temas/gestion-ambiental/impacto-ambiental-y-tipos/definicion-y-objetivo', 1, 0),
(54, NULL, 9, 3, 'Licencia ambiental única', 'Licencia ambiental única', NULL, NULL, '', '', 0, 0, 0, 'https://www.gob.mx/tramites/ficha/licencia-ambiental-unica/SEMARNAT261', 1, 0),
(55, NULL, 9, 3, 'Cédula de operación anual', 'Cédula de operación anual', 'Reporte de emisiones y transferencias de establecimientos de competencia federal: \r\n<ul><li>Fuentes fijas de jurisdicción federal.</li><li>Grandes generadores de residuos peligrosos.</li><li>Prestadores de servicios de manejo de residuos.</li><li>Los que descarguen aguas residuales a cuerpos receptores que sean aguas nacionales.</li><li>Los que generan 25,000 toneladas o más de Bióxido de Carbono Equivalente (tCO2e) de emisiones de Compuestos y Gases Efecto Invernadero (CyGEI) de los sectores productivos establecidos en el Reglamento de la Ley General de Cambio Climático en materia de Registro Nacional de Emisiones (RENE).</li></ul>', '+1 year', '', '', 0, 0, 0, 'https://bit.ly/2uyRUTI', 1, 0),
(56, NULL, 9, 3, 'Registro como generador de residuos peligrosos/ no peligrosos', 'Registro como generador de residuos peligrosos/ no peligrosos', 'Este trámite le permite a las personas físicas y morales que generen residuos peligrosos, que se encuentren en alguna de las categorías de microgenerador, pequeño generador o gran generador registrarse ante la Secretaría de Medio Ambiente y Recursos Naturales (SEMARNAT).', NULL, '', '', 0, 0, 0, NULL, 1, 0),
(57, NULL, 9, 3, 'Dictamen anual de cumplimiento de la NOM-005-ASEA-2016', 'Dictamen anual de cumplimiento de la NOM-005-ASEA-2016', NULL, '+1 year', '', '', 0, 0, 0, NULL, 1, 1),
(58, NULL, 9, 3, 'Registro y autorización de SASISOPA', 'Registro y autorización de SASISOPA', 'Especificaciones y requisitos en materia de seguridad industrial, seguridad operativa y protección al medio ambiente para el diseño, construcción, pre-arranque, operación, mantenimiento, cierre y desmantelamiento de estaciones de servicio con fin específico para el expendio al público de gas licuado de petróleo, por medio del llenado parcial o total de recipientes portátiles a presión.', NULL, '', '', 0, 0, 0, NULL, 1, 0),
(59, NULL, 9, 3, 'Sistema de recuperación de vapores NOM-004-ASEA.2017', 'Sistema de recuperación de vapores NOM-004-ASEA.2017', NULL, '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(60, NULL, 9, 3, 'Auditoria de SASISOPA', 'Auditoria de SASISOPA', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(61, NULL, 9, 3, 'Dictamen de Diseño NOM-005-ASEA-2016', 'Dictamen de Diseño NOM-005-ASEA-2016', NULL, NULL, '', '', 0, 0, 0, 'https://bit.ly/2QxI7aj', 1, 0),
(62, NULL, 9, 3, 'Dictamen de Construcción NOM-005-ASEA-2016', 'Dictamen de Construcción NOM-005-ASEA-2016', NULL, NULL, '', '', 0, 0, 0, 'https://bit.ly/2QxI7aj', 1, 0),
(63, NULL, 9, 3, 'Dictamen de Operación y Mantenimiento NOM-005-ASEA-2016', 'Dictamen de Operación y Mantenimiento NOM-005-ASEA-2016', NULL, NULL, '', '', 0, 0, 0, 'https://bit.ly/2QxI7aj', 1, 0),
(64, NULL, 9, 3, 'Trámite EVIS', 'Trámite Evaluación de Impacto Social (EVIS)', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(65, NULL, 9, 3, 'Mantenimiento', 'Mantenimiento', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 1),
(66, NULL, 1, 3, 'NOM-017-STPS-2008', 'NOM-017-STPS-2008', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(67, NULL, 1, 3, 'NOM-018-STPS-2015', 'NOM-018-STPS-2015', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(68, NULL, 1, 3, 'NOM-019-STPS-2011', 'NOM-019-STPS-2011', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(69, NULL, 1, 3, 'NOM-020-STPS-2011', 'NOM-020-STPS-2011', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(70, NULL, 1, 3, 'NOM-022-STPS-2015', 'NOM-022-STPS-2015', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(71, NULL, 1, 3, 'NOM-025-STPS-2008', 'NOM-025-STPS-2008', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(72, NULL, 1, 3, 'NOM-026-STPS-2008', 'NOM-026-STPS-2008', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(73, NULL, 1, 3, 'NOM-027-STPS-2008', 'NOM-027-STPS-2008', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(78, 1, 1, 3, 'Registro de verificaciones oculares cada 12 meses.', 'Registro de verificaciones oculares cada 12 meses.', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(79, 1, 1, 3, 'Condiciones de seguridad en las áreas de trabajo', 'Condiciones de seguridad en las áreas de trabajo', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(80, NULL, 4, 3, 'Licencia de funcionamiento', 'Licencia de funcionamiento', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(81, NULL, 4, 3, 'Factibilidad de uso de suelo', 'Factibilidad de uso de suelo', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(82, NULL, 6, 1, 'Constancia de situación fiscal', 'Constancia de situación fiscal', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(83, NULL, 6, 3, 'Opinión de cumplimiento ', 'Opinión de cumplimiento ', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(84, NULL, 6, 3, 'Declaración anual del ejercicio anterior ISR', 'Declaración anual del ejercicio anterior ISR', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(85, NULL, 6, 3, 'Declaración anual del ejercicio anterior IEPS', 'Declaración anual del ejercicio anterior IEPS', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(86, NULL, 6, 3, 'Declaración anual del ejercicio anterior IVA', 'Declaración anual del ejercicio anterior IVA', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(87, NULL, 9, 3, 'Auditoria de SASISOPA', 'Auditoria de SASISOPA', NULL, '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(88, NULL, 10, 3, 'Evaluación del impacto social', 'Evaluación del impacto social', NULL, NULL, '', '', 0, 0, 0, 'https://www.gob.mx/tramites/ficha/evaluacion-de-impacto-social/SENER2561', 1, 0),
(89, 2, 1, 3, 'Croquis, plano o mapa general del centro de trabajo, o por áreas que lo integran, actualizado y colocado en los principales lugares de entrada, tránsito, reunión o puntos comunes de estancia o servicios para los trabajadores', 'Croquis, plano o mapa general del centro de trabajo, o por áreas que lo integran, actualizado y colocado en los principales lugares de entrada, tránsito, reunión o puntos comunes de estancia o servicios para los trabajadores', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(90, 2, 1, 3, 'Instrucciones de seguridad aplicables en cada área del centro de trabajo y difundirlas entre los trabajadores, contratistas y visitantes', 'Instrucciones de seguridad aplicables en cada área del centro de trabajo y difundirlas entre los trabajadores, contratistas y visitantes', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(91, 2, 1, 3, 'Plan de atención a emergencias de incendio', 'Plan de atención a emergencias de incendio', NULL, '+1 year', '', '', 1, 0, 0, NULL, 1, 0),
(92, 2, 1, 3, 'Constancias de capacitación a las brigadas de emergencia', 'Constancias de capacitación a las brigadas de emergencia', NULL, '+1 year', '', '', 1, 0, 0, NULL, 1, 0),
(94, 2, 1, 3, 'Registro de simulacros de emergencias de incendio al menos una vez al año.', 'Registro de simulacros de emergencias de incendio al menos una vez al año.', NULL, '+1 year', '', '', 1, 0, 0, NULL, 1, 0),
(95, 2, 1, 3, 'Programa de capacitación anual teórico-práctico en materia de prevención de incendios y atención de emergencias.', 'Programa de capacitación anual teórico-práctico en materia de prevención de incendios y atención de emergencias.', NULL, '+1 year', '', '', 1, 0, 0, NULL, 1, 0),
(96, 2, 1, 3, 'Análisis de grado de riesgo de incendio', 'Análisis de grado de riesgo de incendio', NULL, '+1 year', '', '', 1, 0, 0, NULL, 1, 0),
(97, 3, 1, 3, 'Estudio para analizar el riesgo potencial generado por la maquinaria y equipo', 'Estudio para analizar el riesgo potencial generado por la maquinaria y equipo', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(98, 3, 1, 3, 'Constancias de capacitación a los trabajadores para la operación segura de la maquinaria y equipo', 'Constancias de capacitación a los trabajadores para la operación segura de la maquinaria y equipo', NULL, '+1 year', '', '', 1, 0, 0, NULL, 1, 0),
(99, 4, 1, 3, 'Estudio para analizar los riesgos potenciales de sustancias químicas peligrosas', 'Estudio para analizar los riesgos potenciales de sustancias químicas peligrosas', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(100, 4, 1, 3, 'Manuales de procedimientos para el manejo, transporte y almacenamiento seguro de sustancias químicas peligrosas.', 'Manuales de procedimientos para el manejo, transporte y almacenamiento seguro de sustancias químicas peligrosas.', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(101, 4, 1, 3, 'Manual de primeros auxilios.', 'Manual de primeros auxilios.', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(102, 4, 1, 3, 'Capacitación al personal que brinda primeros auxilios.', 'Capacitación al personal que brinda primeros auxilios.', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(103, 4, 1, 3, 'Instalaciones, equipo o materiales para contener las sustancias químicas peligrosas, para que en el caso de derrame de líquidos o fuga de gases, se impida su escurrimiento o dispersión.', 'Instalaciones, equipo o materiales para contener las sustancias químicas peligrosas, para que en el caso de derrame de líquidos o fuga de gases, se impida su escurrimiento o dispersión.', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(104, 4, 1, 3, 'Autorización para las actividades peligrosas y operaciones en espacios confinados', 'Autorización para las actividades peligrosas y operaciones en espacios confinados', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(105, 4, 1, 3, 'Programa Especifico de Seguridad e Higiene para el Manejo, Transporte y Almacenamiento de Sustancias Químicas Peligrosas.', 'Programa Especifico de Seguridad e Higiene para el Manejo, Transporte y Almacenamiento de Sustancias Químicas Peligrosas.', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(106, 4, 1, 3, 'Capacitación al personal para el uso y manejo de sustancias químicas peligrosas.', 'Capacitación al personal para el uso y manejo de sustancias químicas peligrosas.', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(107, 5, 1, 3, 'Análisis en las áreas en las que se llevarán a cabo los trabajos en altura', 'Análisis en las áreas en las que se llevarán a cabo los trabajos en altura', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(108, 5, 1, 3, 'Autorización por escrito a los trabajadores que realicen trabajos en alturas.', 'Autorización por escrito a los trabajadores que realicen trabajos en alturas.', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(109, 5, 1, 3, 'Programa de revisión y mantenimiento a los sistemas o equipos utilizados para la realización de trabajos en altura', 'Programa de revisión y mantenimiento a los sistemas o equipos utilizados para la realización de trabajos en altura', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(110, 5, 1, 3, 'Instructivos, manuales o procedimientos para la instalación, operación y mantenimiento de los sistemas o equipos utilizados en los trabajos en altura', 'Instructivos, manuales o procedimientos para la instalación, operación y mantenimiento de los sistemas o equipos utilizados en los trabajos en altura', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(111, 5, 1, 3, 'Plan de atención a emergencias derivado de la ejecución de trabajos en alturas.', 'Plan de atención a emergencias derivado de la ejecución de trabajos en alturas.', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(112, 5, 1, 3, 'Capacitación al personal que realiza trabajos en alturas.', 'Capacitación al personal que realiza trabajos en alturas.', NULL, '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(113, 6, 1, 3, 'Reconocimiento y evaluación de ruido.', 'Reconocimiento y evaluación de ruido.', NULL, '+2 years', '', '', 1, 0, 0, NULL, 1, 0),
(114, 6, 1, 3, 'Programa de conservación de la audición aplica en las áreas del centro de trabajo donde se encuentren trabajadores expuestos a niveles de 85 dB(A) y mayores.', 'Programa de conservación de la audición aplica en las áreas del centro de trabajo donde se encuentren trabajadores expuestos a niveles de 85 dB(A) y mayores.', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(115, 66, 1, 3, 'Determinación del equipo de protección personal, de acuerdo a los riesgos de trabajo.', 'Determinación del equipo de protección personal, de acuerdo a los riesgos de trabajo.', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(116, 66, 1, 3, 'Capacitación al personal para el uso, revisión, reposición, limpieza, limitaciones, mantenimiento, resguardo y disposición final del equipo de protección personal', 'Capacitación al personal para el uso, revisión, reposición, limpieza, limitaciones, mantenimiento, resguardo y disposición final del equipo de protección personal', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(117, 67, 1, 3, 'Contar con el sistema armonizado de identificación y comunicación de peligros y riesgos por sustancias químicas peligrosas y mezclas', 'Contar con el sistema armonizado de identificación y comunicación de peligros y riesgos por sustancias químicas peligrosas y mezclas', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(118, 67, 1, 3, 'Implementar en el centro de trabajo, el sistema armonizado de identificación y comunicación de peligros y riesgos por sustancias químicas peligrosas y mezclas.', 'Implementar en el centro de trabajo, el sistema armonizado de identificación y comunicación de peligros y riesgos por sustancias químicas peligrosas y mezclas.', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(119, 67, 1, 3, 'Contar con las hojas de datos de seguridad de todas las sustancias químicas peligrosas y mezclas que se manejen en el centro de trabajo', 'Contar con las hojas de datos de seguridad de todas las sustancias químicas peligrosas y mezclas que se manejen en el centro de trabajo', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(120, 67, 1, 3, 'Señalizar los depósitos, recipientes, anaqueles o áreas de almacenamiento que contengan sustancias químicas peligrosas y mezclas', 'Señalizar los depósitos, recipientes, anaqueles o áreas de almacenamiento que contengan sustancias químicas peligrosas y mezclas', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(121, 67, 1, 3, 'Capacitar a los trabajadores del centro de trabajo que manejan sustancias químicas peligrosas y mezclas, sobre el contenido de las hojas de datos de seguridad y de la señalización', 'Capacitar a los trabajadores del centro de trabajo que manejan sustancias químicas peligrosas y mezclas, sobre el contenido de las hojas de datos de seguridad y de la señalización', NULL, '+1 year', '', '', 0, 0, 0, NULL, 1, 0),
(122, 68, 1, 3, 'Capacitación a los integrantes de la comisión mixta de seguridad e higiene', 'Capacitación a los integrantes de la comisión mixta de seguridad e higiene', NULL, '+1 year', '', '', 1, 0, 0, NULL, 1, 0),
(123, 68, 1, 3, 'Acta constitutiva de la comisión mixta de seguridad e higiene', 'Acta constitutiva de la comisión mixta de seguridad e higiene', NULL, '+1 year', '', '', 1, 0, 0, NULL, 1, 0),
(124, 69, 1, 3, 'Listado actualizado de equipos sujetos a presión.', 'Listado actualizado de equipos sujetos a presión.', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(125, 69, 1, 3, 'Expediente de cada equipos sujeto a presión', 'Expediente de cada equipos sujeto a presión', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(126, 69, 1, 3, 'Programas específicos de revisión y mantenimiento para los equipos clasificados en las categorías II y II', 'Programas específicos de revisión y mantenimiento para los equipos clasificados en las categorías II y II', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(127, 69, 1, 3, 'Procedimientos de operación, revisión y mantenimiento de los equipos', 'Procedimientos de operación, revisión y mantenimiento de los equipos', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(128, 69, 1, 3, 'Determinar y practicar pruebas de presión o exámenes no destructivos a los equipos clasificados en las categorías II y III', 'Determinar y practicar pruebas de presión o exámenes no destructivos a los equipos clasificados en las categorías II y III', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(129, 69, 1, 3, 'Plan de atención a emergencias para los equipos clasificados en las categorías II y III', 'Plan de atención a emergencias para los equipos clasificados en las categorías II y III', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(130, 69, 1, 3, 'Aviso a la STPS de que los equipos que funcionen en su centro de trabajo, clasificados en la Categoría III', 'Aviso a la STPS de que los equipos que funcionen en su centro de trabajo, clasificados en la Categoría III', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(131, 69, 1, 3, 'Capacitar al personal que realiza actividades de operación, mantenimiento, reparación a equipos clasificados en las categorías II y III', 'Capacitar al personal que realiza actividades de operación, mantenimiento, reparación a equipos clasificados en las categorías II y III', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(132, 70, 1, 3, 'Sistemas de pararrayos en las áreas o instalaciones de los centros de trabajo donde se almacenen, manejen o transporten sustancias inflamables o explosivas', 'Sistemas de pararrayos en las áreas o instalaciones de los centros de trabajo donde se almacenen, manejen o transporten sustancias inflamables o explosivas', NULL, NULL, '', '', 0, 0, 0, NULL, 1, 0),
(133, 70, 1, 3, 'Capacitación al personal expuestos a ser cargados electrostáticamente.', 'Capacitación al personal expuestos a ser cargados electrostáticamente.', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(134, 70, 1, 3, 'Medir los valores de resistencia de la red de puesta a tierra.', 'Medir los valores de resistencia de la red de puesta a tierra.', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(135, 71, 1, 3, 'Informe de resultados de la evaluación de los niveles de iluminación', 'Informe de resultados de la evaluación de los niveles de iluminación', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(136, 71, 1, 3, 'Programa de mantenimiento para las luminarias del centro de trabajo', 'Programa de mantenimiento para las luminarias del centro de trabajo', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(137, 72, 1, 3, 'Capacitación a los trabajadores sobre la correcta interpretación de los elementos de señalización.', 'Capacitación a los trabajadores sobre la correcta interpretación de los elementos de señalización.', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(138, 72, 1, 3, 'Ubicar las señales de seguridad e higiene de tal manera que puedan ser observadas e interpretadas por los trabajadores a los que están destinadas', 'Ubicar las señales de seguridad e higiene de tal manera que puedan ser observadas e interpretadas por los trabajadores a los que están destinadas', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(139, 73, 1, 3, 'Análisis de riesgos potenciales para las actividades de soldadura y corte', 'Análisis de riesgos potenciales para las actividades de soldadura y corte', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(140, 73, 1, 3, 'Programa para las actividades de soldadura y corte', 'Programa para las actividades de soldadura y corte', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(141, 73, 1, 3, 'Procedimientos de seguridad para que sean aplicados por los trabajadores que desarrollan actividades de soldadura y corte', 'Procedimientos de seguridad para que sean aplicados por los trabajadores que desarrollan actividades de soldadura y corte', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(142, 73, 1, 3, 'Capacitación a los trabajadores que desarrollan actividades de soldadura y corte', 'Capacitación a los trabajadores que desarrollan actividades de soldadura y corte', NULL, '+1 year', '', '', 1, 0, 0, NULL, 1, 0),
(143, 74, 1, 3, 'Plan de trabajo para los trabajadores que realizan actividades de mantenimiento de las instalaciones eléctricas', 'Plan de trabajo para los trabajadores que realizan actividades de mantenimiento de las instalaciones eléctricas', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(144, 74, 1, 3, 'Diagrama unifilar actualizado de la instalación eléctrica del centro de trabajo, con base en lo dispuesto por la NOM-001-SEDE-2005', NULL, NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(145, 74, 1, 3, 'Procedimientos de seguridad para las actividades de mantenimiento de las instalaciones eléctricas', 'Procedimientos de seguridad para las actividades de mantenimiento de las instalaciones eléctricas', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(146, 74, 1, 3, 'Plan de atención a emergencias', 'Plan de atención a emergencias', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(147, 74, 1, 3, 'Botiquín de primeros auxilios que contenga el manual y los materiales de curación necesarios para atender los posibles casos de emergencia', 'Botiquín de primeros auxilios que contenga el manual y los materiales de curación necesarios para atender los posibles casos de emergencia', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(148, 74, 1, 3, 'Capacitación al personal que realice trabajos de mantenimiento a las instalaciones eléctricas.', 'Capacitación al personal que realice trabajos de mantenimiento a las instalaciones eléctricas.', NULL, '+1 year', '', '', 1, 0, 0, NULL, 1, 0),
(149, 75, 1, 3, 'Análisis de riesgos del espacio confinado', 'Análisis de riesgos del espacio confinado', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(150, 75, 1, 3, 'Procedimientos de seguridad para: las actividades a desarrollar', 'Procedimientos de seguridad para: las actividades a desarrollar', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(151, 75, 1, 3, 'Plan de trabajo específico para realizar trabajos en espacios confinados', 'Plan de trabajo específico para realizar trabajos en espacios confinados', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(152, 75, 1, 3, 'Autorización por escrito para realizar trabajos en espacios confinados', 'Autorización por escrito para realizar trabajos en espacios confinados', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0),
(153, 75, 1, 3, 'Capacitación a los trabajadores que realizan trabajos en espacios confinados.', 'Capacitación a los trabajadores que realizan trabajos en espacios confinados.', NULL, NULL, '', '', 1, 0, 0, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresas`
--

CREATE TABLE `empresas` (
  `id_empresa` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estaciones`
--

CREATE TABLE `estaciones` (
  `id_estacion` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `no_permiso` varchar(255) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `rfc` varchar(13) NOT NULL,
  `razon_social` varchar(255) NOT NULL,
  `calle` varchar(255) NOT NULL,
  `no_ext` varchar(10) NOT NULL,
  `no_int` varchar(10) NOT NULL,
  `colonia` varchar(255) NOT NULL,
  `cp` varchar(10) NOT NULL,
  `municipio` varchar(255) NOT NULL,
  `estado` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `eliminado` tinyint(1) NOT NULL DEFAULT '0',
  `creado_por` int(10) UNSIGNED NOT NULL,
  `eliminado_por` int(10) UNSIGNED NOT NULL,
  `fecha_creacion` int(11) DEFAULT NULL,
  `fecha_eliminacion` int(11) DEFAULT NULL,
  `nombre_representante` varchar(255) DEFAULT NULL,
  `nombre_encargado` varchar(255) DEFAULT NULL,
  `telefono` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estaciones`
--

INSERT INTO `estaciones` (`id_estacion`, `nombre`, `no_permiso`, `logo`, `rfc`, `razon_social`, `calle`, `no_ext`, `no_int`, `colonia`, `cp`, `municipio`, `estado`, `activo`, `eliminado`, `creado_por`, `eliminado_por`, `fecha_creacion`, `fecha_eliminacion`, `nombre_representante`, `nombre_encargado`, `telefono`, `email`) VALUES
(1, 'Puebla', '1000', 'logo.png', 'XAXX010101000', 'G500 S.A. de C.V.', 'Avenida 25 Oriente esquina con la calle 18 Sur.', '', '', '', '', '', '', 1, 0, 1, 1, 1538176354, NULL, 'Juanito Gonzalez', 'Juan Torombon', '', ''),
(5, 'Repsol', '123456', NULL, 'XAXX10101010', 'Repsol de Puebla S.A. de C.V.', 'Calle Siempre Viva No.12', '', '', '', '', '', '', 1, 0, 1, 1, 1538160576, NULL, 'Leonardo Luna', 'Leonardo Luna', '', ''),
(14, 'Tlaxcala', '98765', NULL, 'XAXX101010101', 'DEMO', 'Demostración', '', '', '', '', '', '', 1, 0, 1, 1, 1538175342, NULL, 'Fulano Juarez', 'Mengano Sanchez', '', ''),
(15, 'Veracruz', '98765', NULL, 'XAXX101010101', 'DEMO', 'Demostración', '', '', '', '', '', '', 1, 0, 1, 1, 1538489085, NULL, 'Luis Morales', 'Francisco Rolón', '', ''),
(16, 'México', 'E0567', NULL, 'BEEA821428J92', 'Mi estación de servicio', 'Mi dirección', '', '', '', '', '', '', 1, 0, 1, 1, 1538488829, NULL, 'Carlos Durán Barbosa', 'Isabel Sanchez Corral', '', ''),
(17, 'Prueba', 'ABC1', NULL, 'BSEFUISEFLEF9', 'PRUEBA', '7 Poniente', '744', '5', 'Centro', '73564', 'Puebla', 'Puebla', 1, 0, 1, 1, 1547480656, NULL, 'Prueba', 'Preuba2', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estaciones_has_documentos`
--

CREATE TABLE `estaciones_has_documentos` (
  `id_estacion` int(10) UNSIGNED NOT NULL,
  `id_documento` int(11) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `fecha_documento` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `eliminado` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estaciones_has_documentos`
--

INSERT INTO `estaciones_has_documentos` (`id_estacion`, `id_documento`, `url`, `fecha_documento`, `activo`, `eliminado`) VALUES
(0, 0, '1540232206.pdf', 0, 1, 0),
(1, 7, '1539381914.pdf', 0, 1, 0),
(1, 11, '1539627439.pdf', 0, 1, 0),
(1, 12, '1539382110.pdf', 0, 1, 0),
(1, 13, '1539627537.pdf', 0, 1, 0),
(1, 15, '1539382283.pdf', 0, 1, 0),
(1, 16, '1539382314.pdf', 0, 1, 0),
(1, 17, '1539627823.pdf', 0, 1, 0),
(1, 20, '1539380649.pdf', 0, 1, 0),
(1, 23, '1539380848.pdf', 0, 1, 0),
(1, 24, '1538762836.pdf', 0, 1, 0),
(1, 45, '1539381635.pdf', 0, 1, 0),
(1, 50, '1539626170.pdf', 0, 1, 0),
(1, 53, '1539361039.pdf', 0, 1, 0),
(1, 54, '1539381016.pdf', 0, 1, 0),
(1, 55, '1539381056.pdf', 0, 1, 0),
(1, 56, '1539381248.pdf', 0, 1, 0),
(1, 63, '1539383755.pdf', 0, 1, 0),
(1, 64, '1539635794.pdf', 0, 1, 0),
(1, 80, '1539187338.pdf', 0, 1, 0),
(1, 88, '1539635995.pdf', 0, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `etapas`
--

CREATE TABLE `etapas` (
  `id_etapa` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `eliminado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `etapas`
--

INSERT INTO `etapas` (`id_etapa`, `nombre`, `activo`, `eliminado`) VALUES
(1, 'DISEÑO', 1, 0),
(2, 'CONSTRUCCIÓN', 1, 0),
(3, 'OPERACIÓN', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE `grupos` (
  `id_grupo` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`id_grupo`, `nombre`, `descripcion`, `activo`) VALUES
(1, 'super_administrador', 'Usuario con todos los privilegios', 1),
(2, 'principal', 'Usuario con todos los permisos de acuerdo a su empresa', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `paterno` varchar(255) DEFAULT NULL,
  `materno` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `es_principal` tinyint(1) NOT NULL DEFAULT '1',
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `id_empresa` int(10) UNSIGNED DEFAULT NULL,
  `eliminado` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `email`, `password`, `paterno`, `materno`, `nombre`, `es_principal`, `activo`, `id_empresa`, `eliminado`) VALUES
(1, 'admin@admin.com', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'del', 'sistema', 'Administrador', 1, 1, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_has_estaciones`
--

CREATE TABLE `usuarios_has_estaciones` (
  `id_usuario` int(10) UNSIGNED NOT NULL,
  `id_estacion` int(10) UNSIGNED NOT NULL,
  `id_grupo` int(10) UNSIGNED NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios_has_estaciones`
--

INSERT INTO `usuarios_has_estaciones` (`id_usuario`, `id_estacion`, `id_grupo`, `activo`) VALUES
(1, 1, 1, 1),
(1, 14, 1, 1),
(1, 15, 1, 1),
(1, 16, 1, 1),
(1, 17, 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indices de la tabla `dependencias`
--
ALTER TABLE `dependencias`
  ADD PRIMARY KEY (`id_dependencia`);

--
-- Indices de la tabla `documentos`
--
ALTER TABLE `documentos`
  ADD PRIMARY KEY (`id_documento`),
  ADD KEY `fk_documentos_dependencias1_idx` (`id_dependencia`),
  ADD KEY `fk_documentos_etapas1_idx` (`id_etapa`);

--
-- Indices de la tabla `empresas`
--
ALTER TABLE `empresas`
  ADD PRIMARY KEY (`id_empresa`);

--
-- Indices de la tabla `estaciones`
--
ALTER TABLE `estaciones`
  ADD PRIMARY KEY (`id_estacion`,`eliminado_por`),
  ADD KEY `fk_estaciones_usuarios1_idx` (`creado_por`),
  ADD KEY `fk_estaciones_usuarios2_idx` (`eliminado_por`);

--
-- Indices de la tabla `estaciones_has_documentos`
--
ALTER TABLE `estaciones_has_documentos`
  ADD PRIMARY KEY (`id_estacion`,`id_documento`);

--
-- Indices de la tabla `etapas`
--
ALTER TABLE `etapas`
  ADD PRIMARY KEY (`id_etapa`);

--
-- Indices de la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`id_grupo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `fk_usuarios_empresas1_idx` (`id_empresa`);

--
-- Indices de la tabla `usuarios_has_estaciones`
--
ALTER TABLE `usuarios_has_estaciones`
  ADD PRIMARY KEY (`id_usuario`,`id_estacion`,`id_grupo`),
  ADD KEY `fk_usuarios_has_estaciones_estaciones1_idx` (`id_estacion`),
  ADD KEY `fk_usuarios_has_estaciones_usuarios1_idx` (`id_usuario`),
  ADD KEY `fk_usuarios_has_estaciones_grupos1_idx` (`id_grupo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `dependencias`
--
ALTER TABLE `dependencias`
  MODIFY `id_dependencia` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `documentos`
--
ALTER TABLE `documentos`
  MODIFY `id_documento` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;
--
-- AUTO_INCREMENT de la tabla `empresas`
--
ALTER TABLE `empresas`
  MODIFY `id_empresa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `estaciones`
--
ALTER TABLE `estaciones`
  MODIFY `id_estacion` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `etapas`
--
ALTER TABLE `etapas`
  MODIFY `id_etapa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `grupos`
--
ALTER TABLE `grupos`
  MODIFY `id_grupo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `documentos`
--
ALTER TABLE `documentos`
  ADD CONSTRAINT `fk_documentos_dependencias1` FOREIGN KEY (`id_dependencia`) REFERENCES `dependencias` (`id_dependencia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_documentos_etapas1` FOREIGN KEY (`id_etapa`) REFERENCES `etapas` (`id_etapa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `estaciones`
--
ALTER TABLE `estaciones`
  ADD CONSTRAINT `fk_estaciones_usuarios1` FOREIGN KEY (`creado_por`) REFERENCES `usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_estaciones_usuarios2` FOREIGN KEY (`eliminado_por`) REFERENCES `usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_empresas1` FOREIGN KEY (`id_empresa`) REFERENCES `empresas` (`id_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios_has_estaciones`
--
ALTER TABLE `usuarios_has_estaciones`
  ADD CONSTRAINT `fk_usuarios_has_estaciones_estaciones1` FOREIGN KEY (`id_estacion`) REFERENCES `estaciones` (`id_estacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_has_estaciones_grupos1` FOREIGN KEY (`id_grupo`) REFERENCES `grupos` (`id_grupo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_has_estaciones_usuarios1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
